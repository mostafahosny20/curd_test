<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Events\OrderStatusChange;
use App\Events\SendPosition;
use Modules\OrderModule\Entities\Order;
use Illuminate\Http\Request;

Route::get('/', function () {


     return view('home');
})->name('home');

Route::get('/fire', function () {

$order=Order::find(75);
  event(new OrderStatusChange($order,1));

    // return view('home');
});

Route::post('/fire2', function (Request $request) {

$location=['lat'=>$request->lat,'lng'=>$request->lng];
  event(new SendPosition($location));


    // return view('home');
});



//Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
//
// Route::resource('/task', 'TaskController');
//
// Route::get('/products', function (){
//
//    return response()->json(['data'=>
//        ['id'=>1,'productImage'=>'https://images-na.ssl-images-amazon.com/images/I/91zwD%2B7G9zL._SY879_.jpg','productName'=>'RealMe 1 (Black, 6GB RAM, 128GB Storage).','productDescription'=>'it Camera: 13 MP Rear camera with Fast facial unlock in less than 0.1 second with AI Recognition | 8 MP front camera Display: 15.24 centimeters (6-inch) Full HD In-cell capacitive touchscreen display with 2160x1080 pixels.','productCategory'=>'Mobile','productSeller'=>'Samsung'],
//        ['id'=>1,'productImage'=>'https://images-na.ssl-images-amazon.com/images/I/616-sGx5LTL._SY879_.jpg','productName'=>'Halxcy','productDescription'=>'it very easy to use','productCategory'=>'Mobile','productSeller'=>'Samsung']
//    ]);
// });
