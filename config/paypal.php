<?php

return array(

    /**
     * Set our Sandbox and Live credentials
     */
    'client_id' =>'AbNiMQBPUnMY0gAF56cE-mMRqX-QBD1cchfTf3TopUBYEz-JG7e_h461WxwPjfsHo-HSvpWgfLCgIhZt',
    'secret' => 'ECp3sFYIzXQATnTZG37SqPoTkoftZC9Cg3YSWKgM2MZjp-gPptIfkmUEvQ57wjmt99oXMcWBvp40ylIx',


    /**
     * SDK configuration settings
     */
    'settings' => array(

        /**
         * Payment Mode
         *
         * Available options are 'sandbox' or 'live'
         */
        'mode' => env('PAYPAL_MODE', 'sandbox'),

        // Specify the max connection attempt (3000 = 3 seconds)
        'http.ConnectionTimeOut' => 30,

        // Specify whether or not we want to store logs
        'log.LogEnabled' => true,

        // Specigy the location for our paypal logs
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Log Level
         *
         * Available options: 'DEBUG', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the DEBUG level and decreases
         * as you proceed towards ERROR. WARN or ERROR would be a
         * recommended option for live environments.
         *
         */
        'log.LogLevel' => 'DEBUG'
    ),
);
