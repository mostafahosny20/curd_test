<?php

namespace App;
use App\Product;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    //
    public function products(){
      return $this->hasMany(Product::class,'cat_id');
    }
}
