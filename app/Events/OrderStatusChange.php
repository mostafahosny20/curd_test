<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Modules\OrderModule\Entities\Order;

class OrderStatusChange implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $order;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {

      $this->order=$order;
        //
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
          // return new PrivateChannel('track_order.'.$this->order->id);
      return new Channel('pizza-tracker.'.$this->order->id,'pizza-tracker');
      // return new Channel('tracker');

    }


    public function broadcastWith()
    {
        $extra = [
            'order' => $this->order,
        ];
        return  $extra;
    }

}
