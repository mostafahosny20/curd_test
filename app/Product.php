<?php

namespace App;
use App\Categories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
  use SoftDeletes;
  protected $fillable=['cat_id','name_ar','name_en','desc_ar','desc_en','image'];

  public function categories(){
    return $this->belongsTo(Categories::class,'cat_id');
  }

  public function prices(){
    return $this->hasMany('\App\Prices','product_id');
  }

    //
}
