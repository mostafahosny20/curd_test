<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ReceiptTypes extends Enum
{
    const IN = "سند استلام";
}
