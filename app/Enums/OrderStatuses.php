<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class OrderStatuses extends Enum
{
    const _NEW = 1;
    const UNDER_PREPARATION = 2;
    const READY = 3;
    const UNDER_SHIPPING = 4;
    const SHIPPED = 5;
    const DONE = 6;
    const CANCELED = 7;
}
