<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class OrderStatusType extends Enum
{
    const OPEN = 1;
    const COMPLETED = 2;
    const CANCELED = 3;
    const DELAYED = 4;

}
