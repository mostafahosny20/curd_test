this.orderMoney.sub_total=this.totalValue;
this.orderMoney.total=this.totalValue+this.deliveryCost;
this.orderMoney.shipping=this.deliveryCost;
this.orderProducts=this.cartProducts;
this.orderDetails.user_address_id=this.address.id;
this.orderDetails.delivery_ship_price=30;

axios
  .post(`api/order`,{
    orderMoney:this.orderMoney,
    orderProducts:this.orderProducts,
    orderDetails:this.orderDetails

  })
  .then(response => {
    this.showLoader = false;

    if(response.data.code==200){
    successToaster('order saved', "");
    console.log(response.data);
    }
    else{errorToaster(response.data.message, "");}
  })
  .catch(error => {
    this.showLoader = false;

    errorToaster('internal error', "");

  });




  calulateTotalPrice() {
    this.total = 0;
    this.cartProducts.forEach(product => {
      product.product_prices.forEach(price=>{
      this.total += parseFloat(price.price*price.qua);
      });

    });
    this.total += this.deliveryCost;
  }



// checkout
  removeProductCart(product) {
    const products = JSON.parse(localStorage.getItem("iki-cart"));
    for (let i = 0; i < products.length; i++) {
      if (products[i]._id === product._id) {
        products.splice(i, 1);
      }
    }
    this.SET_CART_PRODUCTS(products);
    localStorage.setItem("iki-cart", JSON.stringify(products));
    this.$refs.cartCalculator.calulateTotalPrice();
  }
