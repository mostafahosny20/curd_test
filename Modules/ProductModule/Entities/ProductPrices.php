<?php

namespace Modules\ProductModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class ProductPrices extends Model
{
  protected $fillable =
  [
      'price',
      'size',
      'units',
      'product_id'
  ];

  /**
  *  'mcrypt_enc_get_supported_key_sizes',
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'product_prices';

  public function product()
  {
      return $this->belongsTo(Product::class);
  }

}
