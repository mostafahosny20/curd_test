<?php

namespace Modules\ProductModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class ProductCategory extends Model
{
  protected $table = 'product_categories';

  use Translatable;
  public $translatedAttributes = ['title', 'description', 'meta_title', 'meta_desc', 'meta_keywords', 'slug'];

  protected $fillable = ['parent_id', 'photo'];

  public $translationModel = ProductCategoryTranslation::class;


  function products(){
    return $this->hasMany(Product::class);
  }

  function active_products()
  {
      return $this->hasMany(Product::class, 'product_category_id')
          ->where('active', 1)->withTimestamps();
  }
}
