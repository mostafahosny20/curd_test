<?php

namespace Modules\ProductModule\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Product extends Model
{

  use Translatable;

  protected $table = 'products';
  protected $fillable = ['photo'];
  public $translatedAttributes = ['title','slug', 'description', 'meta_title', 'meta_desc', 'meta_keywords'];
  public $translationModel = ProductTranslation::class;


   function product_category(){
     return $this->belongsTo(ProductCategory::class,'product_category_id');
   }

  function product_prices(){
    return $this->hasMany(ProductPrices::class);
  }

}
