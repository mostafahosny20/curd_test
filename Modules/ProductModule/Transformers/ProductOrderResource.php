<?php

namespace Modules\ProductModule\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ProductOrderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            "id" => $this->id,
            "thumb_photo" => asset('public/images/products/thumb/'.$this->photo),
            "price" => $this->sell_price,
            "quantity"=> $this->pivot->quantity,
            'sub_category_id'=>$this->categories,
            'ar' => $this->translate('ar'),
            'en' => $this->translate('en'),
        ];
    }

}
