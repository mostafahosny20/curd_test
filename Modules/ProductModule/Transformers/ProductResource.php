<?php

namespace Modules\ProductModule\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Modules\ProductModule\Transformers\CategoryResource;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
          return [
              'id' => $this->id,
              "photo" => asset('public/images/products/'.$this->photo),
              'title'=>$this->title,
              'description'=>$this->description,
              'product_prices' => $this->product_prices,
              'product_category' => new CategoryResource($this->product_category),
              // 'ar' => $this->translate('ar'),
              // 'en' => $this->translate('en'),



            // "id" => $this->id,
            // "thumb_photo" => asset('public/images/products/thumb/'.$this->photo),
            // "price" => $this->sell_price,
            // "current_quantity"=> $this->current_quantity,  ## Cause ERROR (trying to get property 'quantity' of non object).
            // //'category'=>$this->categories,
            // 'ar' => $this->translate('ar'),
        ];
    }

}
