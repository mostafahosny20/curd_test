<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('products', function (Blueprint $table) {
          $table->increments('id');
          $table->string('photo')->nullable();
          $table->integer('product_category_id')->unsigned()->nullable();
          $table->integer('active')->default(1)->nullable();
         // $table->foreign('product_category_id')->references('id')->on('product_categories')->onDelete('cascade');

          $table->timestamps();
      });

      # Translation
      Schema::create('product_translations', function (Blueprint $table) {
          $table->increments('id');
          $table->string('title');
          $table->text('description')->nullable();
          $table->integer('product_id')->unsigned()->nullable();
          $table->string('meta_title')->nullable();
          $table->text('meta_desc')->nullable();
          $table->string('meta_keywords')->nullable();
          $table->string('slug')->nullable();
          $table->string('locale')->index();
          $table->unique(['product_id', 'locale']);
          $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('products');
      Schema::dropIfExists('products_translations');
    }
}
