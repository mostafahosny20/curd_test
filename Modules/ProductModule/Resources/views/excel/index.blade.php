@extends('commonmodule::layouts.master')

@section('title')
  {{__('commonmodule::sidebar.excel')}}
@endsection

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
 @endsection

@section('content-header')
  <section class="content-header">
    <h1>
      {{__('commonmodule::sidebar.excel')}}
    </h1>
  </section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{{__('commonmodule::sidebar.excel')}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form class="form-horizontal" action="{{ url('/admin-panel/import') }}" enctype="multipart/form-data" method="POST">
                @csrf
                <div class="form-group">
                    <label class="control-label col-sm-2" for="file">{{__('productmodule::product.file')}}:</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="file" name="file" class="form-control">
                    </div>
                    <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary pull-right">{{__('productmodule::product.upload')}} &nbsp; <i class="fa fa-save"></i></button>
                    </div>
                </div>
                  <div class="box-footer">
                    <a href="{{url('/admin-panel/product')}}" type="button" class="btn btn-default pull-right">{{__('productmodule::product.cancel')}} &nbsp; <i class="fa fa-remove" aria-hidden="true"></i> </a>

                  </div>
                  <!-- /.box-footer -->
            </form>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
@endsection

@section('javascript')

@endsection
