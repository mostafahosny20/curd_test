@extends('commonmodule::layouts.master')

@section('title')
  {{__('productmodule::product.pagetitle')}}
@endsection

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
 @endsection

@section('content-header')
  <section class="content-header">
    <h1>
      {{__('productmodule::product.pagetitle')}}
    </h1>
  </section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{{__('productmodule::product.pagetitle')}}</h3>
          {{-- Add New --}}
          <a href="{{url('admin-panel/product/create')}}" type="button" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; {{__('productmodule::product.addnew')}}</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="productIndex" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>{{__('productmodule::product.id')}}</th>
                <th>{{__('productmodule::product.name')}}</th>
                <th>{{__('productmodule::product.price')}}</th>
                <th>{{__('productmodule::product.update')}}</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
@endsection

@section('javascript')

  {{-- sweet alert --}}
  <script src="{{asset('assets/admin/plugins/sweetalert/sweetalert.min.js')}}"></script>

{{-- apply ajax call --}}
<script>
$(document).ready(function() {
    $(document).on('click', '#update_price', function(e) {
        var $tr = $(this).closest('tr')
        id = $tr.find('#product_id').val()
        price = $tr.find('#sell_price').val()

        $.ajax({
            'type': 'post',
            'url': '{{ url("admin-panel/product/update-price") }}',
            data: {
                '_token': '{{csrf_token()}}',
                'id': id,
                'price': price
            },
            'statusCode': {
                    200: function (response) {
                        swal("رائع", "تم تحديث سعر المنتج بنحاح!", "success", { button: "Ok", });
                    },
                    422: function (response) {
                        swal("خطأ", "حدث خطأ ما، أعد ادخال البيانات الصحيحة", "error", { button: "Ok", });
                    }
                },
        })
    })
})
</script>


  <script src="{{asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

  <script>
      $(document).ready(function () {

          $('#productIndex').DataTable({
              dom: 'lBfrtip',
              buttons: [
                  { extend: 'print', text: 'طباعه',messageBottom:' <strong>جميع الحقوق محفوظة  Makdak .</strong>'},
                  { extend: 'excel', text: ' اكسيل' },
              ] ,
              "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
              "language": {
                  "search" : "  بحث :  ",
                  "paginate": {
                      "previous": "السابق",
                      "next": "التالى"
                  },
                  "info":           "عرض _START_ الي _END_ من _TOTAL_ من الصفوف",
                  "lengthMenu":     "عرض _MENU_ من الصفوف",
                  "loadingRecords": "جاري التحميل...",
                  "processing":     "جاري التحميل...",
                  "zeroRecords":    "لا يوجد نتائج",
                  "infoEmpty":      "عرض 0 to 0 of 0 من الصفوف",
                  "infoFiltered":   "(عرض من _MAX_ صف)",
              } ,
              "processing": true,
              "serverSide": true,
              "ajax": {
                  "url": "{{ url('admin-panel/edit-product-price/ajax') }}",
                  "type": "GET"
              },
              "columns": [
                  { data: 'id', name: 'id' },
                  { data: 'name', name: 'name' },
                  { data: 'price', name: 'price' },
                  { data: 'update', name: 'update', orderable: false, searchable: false},

              ]
          });

      });
  </script>
@endsection
