@extends('commonmodule::layouts.master')

@section('title')
  {{__('productmodule::product.pagetitle')}}
@endsection

@section('css')
  <link rel="stylesheet" href="{{ asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
 @endsection

@section('content-header')
  <section class="content-header">
    <h1>
      {{__('productmodule::product.pagetitle')}}
    </h1>
  </section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{{__('productmodule::product.pagetitle')}}</h3>
          {{-- Add New --}}
          <a href="{{url('admin-panel/product/create')}}" type="button" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; {{__('productmodule::product.addnew')}}</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="productIndex" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>{{__('productmodule::product.id')}}</th>
                <th>{{__('productmodule::product.product')}}</th>
                <th>{{__('productmodule::product.photo')}}</th>
                <th>{{__('productmodule::product.price')}}</th>
                <th>الاقسام</th>
                  <th>{{__('productmodule::product.edit')}}</th>
                <th>{{__('productmodule::product.status')}}</th>
              </tr>
            </thead>
            <tbody>
              @foreach($products as $product)

                <tr>
                  <td>{{$product->id}}</td>
                  <td>{{$product->title}}</td>
                  <td>{{$product->photo}}</td>
                  <td>{{$product->product_prices[0]->price}}</td>
                  <td>{{$product->product_category->title}}</td>
                  <td><a href="{{url('productmodule/product/'.$product->id)}}" class="btn btn-success"><i class="fa fa-eye"></i> </a></td>
                  <td><a href="{{url('productmodule/product/'.$product->id.'/edit')}}" class="btn btn-primary"><i class="fa fa-edit"></i> </a></td>

                </tr>

              @endforeach

            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
@endsection


@section('javascript')


    @include('commonmodule::includes.swal')

    <!-- DataTables -->
    <script src="{{asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#Index').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            });
        })

    </script>

@endsection
