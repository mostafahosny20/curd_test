<?php

return array(

  'pagetitle' => 'Products',
  'homepage' => 'Home Page',
  'addnew' => 'Add New',
  'id' => 'ID',
  'product' => 'Product',
  'photo' => 'Photo',
  'operation' => 'Operations',
  'edit' => 'Edit',
  'delete' => 'Delete',
  'title' => 'Title',
  'price' => 'Price',
  'categs' => 'Choose Category or more',
  'desc' => 'Description',
  'album' => 'Upload Album',
  'mt' => 'Meta Title',
  'md' => 'Meta Desc',
  'tags' => 'Tags',
  'cancel' => 'Cancel',
  'submit' => 'Submit',
  'volume' => 'Volume',
  'measure' => 'Measure',
  'num_of_item' => 'Number of items',
  'item_cost' => 'Item Cost',
  'upload' => 'Upload',
  'file'   => 'File',
  'name' => 'Product Price',
  'update' => 'update'
);
