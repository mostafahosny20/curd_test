<?php

namespace Modules\ProductModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\productmodule\Repository\ProductCategoryRepository;
use Modules\productmodule\Repository\ProductRepository;

class ProductModuleController extends Controller
{

    protected $ProductCategoryRepository;
    protected $ProductRepository;
     public function __construct(ProductCategoryRepository $ProductCategoryRepository, ProductRepository $ProductRepository){
         $this->ProductCategoryRepository = $ProductCategoryRepository;
         $this->ProductRepository =$ProductRepository;
     }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $products=$this->ProductRepository->findAll();
        return view('productmodule::product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories=$this->ProductCategoryRepository->getAll();
        return view('productmodule::product.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $this->ProductRepository->save($request);
        return redirect('productmodule/product');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('productmodule::product.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $product=$this->ProductRepository->find($id);
        $categories=$this->ProductCategoryRepository->getAll();
        return view('productmodule::product.edit',compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function getData(){
      return DataTables::of($this->ProductRepository->findAll())
      ->addColumn('id',function($item){
        return $item->id ?? '';
      })
      ->addColumn('name',function($item){
        return $item->title ?? '';
      })
      ->addColumn('photo',function($item){
        return $item->photo ?? '';
      })
      ->addColumn('price',function($item){
        return $item->product_prices->price ?? '';
      })
      ->addColumn('category',function($item){
        return $item->product_category->title ?? '';
      })
      ->addColumn('view',function($item){
        return '<a href="' .url("productmodule/product/".$item->id) .'" type="button" class="btn btn-success"><i class="fa fa-eye" aria-hidden="true" ></i> </a>';
      })
      ->addColumn('edit',function($item){
        return '<a href="' .url("productmodule/product/".$item->id ."/edit") .'" type="button" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true" ></i> </a>';
      })
      ->rawColumn(['edit'=>'edit' , 'view'=>'view'])
      ->make(true);
    }

}
