<?php

namespace Modules\ProductModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\ProductModule\Repository\ProductCategoryRepository;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
     protected $ProductCategoryRepository;
     public function __construct(ProductCategoryRepository $ProductCategoryRepository){
         $this->ProductCategoryRepository = $ProductCategoryRepository;
     }
    public function index()
    {
        $categories=$this->ProductCategoryRepository->getAll();
        return view('productmodule::category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories=$this->ProductCategoryRepository->getAll();
        return view('productmodule::category.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        $categoryData=$request->all();
        $this->ProductCategoryRepository->saveCategory($categoryData,$request->file('photo'));
        return redirect('productmodule/product-categories');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $category=$this->ProductCategoryRepository->getCategoryById($id);
        return view('productmodule::category.show',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
      $category=$this->ProductCategoryRepository->getCategoryById($id);
      $categories=$this->ProductCategoryRepository->getAll();
      return view('productmodule::category.edit',compact('category','categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $categoryData=$request;
        $this->ProductCategoryRepository->updateCategory($categoryData);
        return redirect('productmodule/product-categories');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
