<?php

namespace Modules\ProductModule\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\ProductModule\Repository\ProductRepository;
use Modules\ProductModule\Transformers\CategoryCollection;
use Modules\ProductModule\Transformers\ProductCollection;
use Modules\CommonModule\Helper\ApiResponseHelper;
use Illuminate\Support\Facades\DB;
use Modules\OrderModule\Entities\OrderProduct;
use Modules\OrderModule\Entities\Product;

class ProductApiController extends Controller
{

    use ApiResponseHelper;

   public function __construct(ProductRepository $prodreop){
      $this->product_repositery=$prodreop;
   }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

      $products= $this->product_repositery->findAll();

      $resource_products=new ProductCollection($products);

        return $this->setCode(200)->setData($resource_products)->send();
    }

    public function activeProducts(){
      $products= $this->product_repositery->findAllActive()->groupBy('product_category.id');

      // $resource_products=new ProductCollection($products);
      //  $data=['products'=>$resource_products,'product_category_group'=>$resource_products->groupBy('product_category.title')];
      return $this->setCode(200)->setData($products)->send();

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('productmodule::create');
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

      return $this->setCode(200)->setData($id)->send();

    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('productmodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function bestSeller(){



      $producttemp=OrderProduct::groupBy("product_id")->orderByRaw('SUM(qua) DESC')->limit(6)->with('product')->get();
      // $products=DB::table('order_products')
      //           select("product_id, SUM(qua) AS TotalQuantity")
      //           ->groupBy("product_id")->orderByRaw('SUM(points) DESC')->get();

      // $products=DB::select("SELECT  product_id, SUM(qua) AS TotalQuantity
      //            FROM order_products GROUP BY product_id
      //            ORDER BY SUM(qua) DESC limit 5");
      $products=[];
      foreach ($producttemp as $key => $value) {
        $products[$key]['product']=$value->product;
        $products[$key]['product']['prices']=$value->product->product_prices;

      }

     return $this->setCode(200)->setData($products)->send();

    }


    public function searchByName($word){
      $products= $this->product_repositery->searchByName($word);
        return $this->setCode(200)->setData($products)->send();

    }


    public function tests(){
      $arr=[];
      $data2[0] = [

          "type"=> "banner_content",
          "marginTop"=> 25,
          "components"=> [
            [
              "type"=> "banner",
              "source"=> "https://cravedfw.files.wordpress.com/2018/02/101-pizza.jpg",
              "category_id"=> 1,
              "height"=> 150,
              "col"=> 1
            ]
          ]


        ];

      $data2[2] = [

        "type" =>"slider",
        "height" => 350,
        "images" => [
                  [
                    "title" => "اشترى بيتزا وخد حاجه ساقعة هدية",
                    "source" => "https://static.olocdn.net/menu/applebees/82a758867027fe0724fd1f078fc9e17c.jpg",
                    "category_id" => 1
                  ],
                  [
                    "title" => "اشترى بيتزا وخد حاجه ساقعة هدية",
                    "source" => "https://static.olocdn.net/menu/applebees/82a758867027fe0724fd1f078fc9e17c.jpg",
                    "category_id" => 1

                  ]
        ]

        ];

    $data1=[
        "id"=> 27,
        "categoryId"=> 27,
        "cover"=> "https://www.southerncastiron.com/wp-content/uploads/2019/06/cast_image005_RGB_1080x1080-1.jpg",
        "images"=> [
          [
            "source"=> "http://wallpaperstock.net/pizza-basil-cheese-vegetables_wallpapers_54017_1920x1080.jpg"
          ],
          [
            "source"=> "http://ptownpizza.com/images/Food/Pizza/Pizza02.jpg"
          ],
          [
            "source"=> "https://cravedfw.files.wordpress.com/2018/02/101-pizza.jpg"
          ]
        ],
        "title"=> "شاورما فراخ",
        "brand"=> 1,
        "shortDescription"=> "فراخ طازجة",
        "description"=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed aliquet semper tortor, commodo tincidunt enim scelerisque ut. Sed in augue sem. Vestibulum facilisis iaculis tortor quis pulvinar. Etiam nisl purus, fermentum quis bibendum sed, blandit ut mauris. Vestibulum eget lobortis tortor. Maecenas volutpat viverra massa eu pulvinar. Ut blandit risus ullamcorper, rhoncus odio quis, ullamcorper quam. Vestibulum lacinia viverra ligula, ut mollis lectus congue eu. Integer eros erat, euismod eget mi eu, vulputate luctus arcu. Maecenas erat velit, tristique id euismod ut, aliquam nec ante. Ut a orci scelerisque metus bibendum pellentesque at aliquet est. Donec vel commodo dolor. Phasellus consectetur lacus sit amet pharetra dignissim.",
        "product_prices"=>[
                            [
                              "id"=> 1,
                              "size"=> "LG",
                              "qua"=> 0,
                              "price"=> 100,

                            ],
                            [
                              "id"=> 2,
                              "size"=> "SM",
                              "qua"=> 0,
                              "price"=> 100,

                            ],

        ],
        "properties"=> [
          [
            "title"=> "Ön (Selfie) Kamera",
            "value"=> "16 MP"
          ],
          [
            "title"=> "Dahili Hafıza",
            "value"=> "64 GB"
          ],
          [
            "title"=> "RAM Kapasitesi",
            "value"=> "4 GB RAM"
          ],
          [
            "title"=> "RAM Kapasitesi",
            "value"=> "4 GB RAM"
          ],
          [
            "title"=> "Pil Gücü",
            "value"=> "3000 mAh"
          ],
          [
            "title"=> "Ön (Selfie) Kamera",
            "value"=> "16 MP"
          ]
        ],
        "percentage"=> 30,
        "price"=> 10,
        "oldPrice"=> 25,
        "freeShipping"=> true

    ];

    $data2[4]=[
      "type"=> "tabs1",
      "categories"=> [
        [
          "id"=> 25,
          "title"=> "شاورما",
          "products"=> [$data1,$data1,$data1,$data1]
        ],
        [
          "id"=> 26,
          "title"=> "بيتزا",
          "products"=> [$data1,$data1,$data1,$data1]
        ],

    ]
  ];

$data2[5]=[
    "type"=> "product_scroll",
    "title"=> "الاكثر مبيعا",
    "products"=> [$data1,$data1,$data1,$data1,$data1,$data1,$data1,$data1,$data1]

];

$data2[6]=  [
    "type"=> "category_banner",
    "categories"=> [
      [
        "id"=> 25,
        "title"=> "بيتزا",
        "source"=> "http://ptownpizza.com/images/Food/Pizza/Pizza02.jpg",
      ],
      [
        "id"=> 26,
        "title"=> "شاورما",
        "source"=> "http://ptownpizza.com/images/Food/Pizza/Pizza02.jpg",
      ],
      [
        "id"=> 27,
        "title"=> "برجر",
        "source"=> "http://ptownpizza.com/images/Food/Pizza/Pizza02.jpg",
      ],
      [
        "id"=> 28,
        "title"=> "فطير",
        "source"=> "http://ptownpizza.com/images/Food/Pizza/Pizza02.jpg",
      ]
    ],
    "col"=> 1,
    "height"=> 250
  ];

//
// $arr=[$data2];


      return $this->setCode(200)->setData($data2)->send();

    }
}
