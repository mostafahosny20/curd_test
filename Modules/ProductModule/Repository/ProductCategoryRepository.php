<?php

namespace Modules\ProductModule\Repository;

use Modules\ProductModule\Entities\ProductCategory;
use Modules\ProductModule\Entities\ProductCategoryTranslation;
use Modules\CommonModule\Helper\LanguageHelper;
use Modules\CommonModule\Helper\UploaderHelper;

class ProductCategoryRepository{

  use UploaderHelper;

  public function getAll(){
    return ProductCategory::all();
  }


  function getCategoryById($id){

    return ProductCategory::find($id);
  }

  public function saveCategory($categoryData,$file){
    $category = new ProductCategory;


    $category->parent_id=$categoryData['parent_id'];  // save parent

    if ($file){
        $name =$this->upload($file,'category');
        $category->photo=$name;
    }
    $category->save();
    // get all activeLang
    $lang = LanguageHelper::getLang();
    foreach ($lang as $language) {
      // code...
      $code=$language->lang;
      $categoryTranslation = new ProductCategoryTranslation;
      $categoryTranslation->product_category_id=$category->id;
      $categoryTranslation->title=$categoryData[$code.'title'];
      $categoryTranslation->description=$categoryData[$code.'description'];
      $categoryTranslation->meta_title=$categoryData[$code.'meta_title'];
      $categoryTranslation->meta_desc=$categoryData[$code.'meta_desc'];
      $categoryTranslation->meta_keywords=$categoryData[$code.'meta_keywords'];
      $categoryTranslation->slug=$categoryData[$code.'slug'];
      $categoryTranslation->locale=$code;
      $categoryTranslation->save();
    }
      return;
  }

  // update category data

  public function updateCategory($categoryData){
  //  dd($categoryData);

    $category=ProductCategory::where('id',$categoryData['id'])->first();

    $category->parent_id=$categoryData['parent_id'];
    if ($file=$categoryData->file('photo')){

      $name =$this->upload($file,'category');
        $category->photo=$name;
    }

    $category->update(['parent_id'=>$category->parent_id , 'photo'=>$category->photo]);

    // get all activeLang
    $lang = LanguageHelper::getLang();
    foreach ($lang as $language) {
      // code...
      $code=$language->lang;
      $categoryTranslation=ProductCategoryTranslation::where('product_category_id',$category->id)->where('locale',$code)->update([

        'title'=> $categoryData[$code]['title'],

        'description'=> $categoryData[$code.'description'],
        'meta_title'=> $categoryData[$code.'meta_title'],
        'meta_desc'=> $categoryData[$code.'meta_desc'],
        'meta_keywords'=>$categoryData[$code.'meta_keywords'] ,
        'slug'=> $categoryData[$code.'slug'],


      ]);

    }

    return;
  }

}







?>
