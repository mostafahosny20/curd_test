<?php

namespace Modules\ProductModule\Repository;
use Modules\ProductModule\Entities\Product;
use Modules\ProductModule\Entities\ProductPrices;
use Modules\ProductModule\Entities\ProductTranslation;
use Modules\CommonModule\Helper\LanguageHelper;

class ProductRepository{

  public function find($id)
  {
<<<<<<< HEAD
    $product=Product::where('id',$id)->with('product_category','product_prices')->first();
    return $product;

=======
    $products=Product::where('id',$id)->with(['product_prices','product_category'])->first();
    return $products;
>>>>>>> dbfe9fd7bbdc563e480e6a5fd6d432f21d080708
  }

  # Index
  public function findAll()
  {
    $products=Product::with(['product_prices','product_category'])->get();
    return $products;

  }

  # Index
  public function findAllActive()
  {
    $products=Product::where('active',1)->with(['product_prices','product_category'])->get();
    return $products;
  }

  public function searchByName($word)
  {
    $products=Product::whereTranslationLike('title','%'.$word.'%')->with(['product_prices','product_category','translations'])->get();
    return $products;
  }



  # Limit 6
  public function find_limit($limit)
  {

  }

  # Insert
  public function save($productData)
  {
    $product= new Product;
    if ($file=$productData->file('photo')){
        $name=time() . $file->getClientOriginalName();
        $file->move('images',$name);
        $product->photo=$name;
    }
    $product->product_category_id=$productData['product_categ'][0];
    $product->active=1;
    //dd($product);
    $product->save();

    //// save data to product prices

    $productPrices=new ProductPrices;
    $productPrices->product_id=$product->id;
    $productPrices->size=$productData['volume'];
    $productPrices->price=$productData['measure'];
    $productPrices->units=$productData['num_of_item'];
    $productPrices->save();


    //// save data to product translation
    $languageObject=new LanguageHelper;
    $lang = $languageObject->getLang();
    foreach ($lang as $language) {
      $code=$language->lang;

      $productTranslation = new ProductTranslation;
      $productTranslation->product_id=$product->id;
      $productTranslation->title=$productData[$code.'title'];
      $productTranslation->description=$productData[$code.'description'];
      $productTranslation->meta_title=$productData[$code.'meta_title'];
      $productTranslation->meta_desc=$productData[$code.'meta_desc'];
      $productTranslation->meta_keywords=$productData[$code.'meta_keywords'];
      $productTranslation->slug=$productData[$code.'slug'];
      $productTranslation->locale=$code;
      $productTranslation->save();
    }

    return;
  }

  public function delete()
  {


  }

  # Edit
  public function update()
  {

  }

  # Update Price through ajax request.
  public function updatePrice()
  {
  }

  # Update Active through ajax request.
  public function updateActive($id, $active)
  {
  }



}







?>
