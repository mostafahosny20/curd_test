<?php
// use Modules\productmodule\Repository\ProductRepository;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('productmodule')->group(function() {
    Route::resource('/product-categories', 'CategoriesController');
    Route::resource('/product', 'ProductModuleController');
    Route::get('/product/ajax','ProductModuleController@getData');
    // Route::get('/product/test',function(){
    //
    //   $product= new ProductRepository;
    //   return $product->findAll();
    // });
});
