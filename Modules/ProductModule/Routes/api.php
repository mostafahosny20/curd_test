<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/productmodule', function (Request $request) {
    return $request->user();
});

// Route::resource('product','Api\ProductApiController');

Route::get('product/allProducts','Api\ProductApiController@index');
Route::post('product/active_products','Api\ProductApiController@activeProducts');

Route::get('product/best_seller','Api\ProductApiController@bestSeller');
Route::get('product/searchproduct/{word}','Api\ProductApiController@searchByName');


Route::get('product/test','Api\ProductApiController@tests');
