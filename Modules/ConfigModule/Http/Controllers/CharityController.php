<?php

namespace Modules\ConfigModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\ConfigModule\Repository\CharityRepository;
use Modules\AreaModule\Entities\City;
use Modules\AreaModule\Entities\Zone;

class CharityController extends Controller
{
  private $charityRepo;

  public function __construct(CharityRepository $charity)
  {
        $this->charityRepo=$charity;
  }

  public function index()
  {
    $charities =   $this->charityRepo->findAll();
    return view('config::charity.index',['charities'=>$charities]);
  }

  public function create()
  {
      $cities=City::all();

      return view('config::charity.create', ['cities' => $cities]);
  }

  public function store(Request $request)
  {
      $data = $request->except('_token');
      $this->charityRepo->save($data);

      return redirect('admin-panel/config-module/charity')->with('success', 'success');
  }

  public function edit($id)
  {
    $cities=City::all();
    $zones=Zone::all();

    $charity = $this->charityRepo->find($id);

    return view('config::charity.edit',['charity'=>$charity,'cities' => $cities,'zones'=>$zones]);
  }

  public function update(Request $request, $id)
  {

    $data = $request->except('_token', '_method');

    $charity = $this->charityRepo->find($id);

    $this->charityRepo->update($data,$charity);

    return redirect('admin-panel/config-module/charity')->with('updated', 'updated');

  }

  public function destroy($id)
  {
    $charity = $this->charityRepo->find($id);
    $this->charityRepo->delete($charity);

    return redirect()->back();
  }

}
