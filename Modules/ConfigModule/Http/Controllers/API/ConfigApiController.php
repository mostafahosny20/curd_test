<?php
/**
 * Created by PhpStorm.
 * User: mallahsoft
 * Date: 27/04/19
 * Time: 05:59 م
 */

namespace Modules\ConfigModule\Http\Controllers\API;


use Illuminate\Routing\Controller;
use Modules\CommonModule\Helper\ApiResponseHelper;
use Modules\ConfigModule\Repository\ConfigRepository;


class ConfigApiController extends Controller
{
    use ApiResponseHelper;


    private $configRepo;

    public function __construct(ConfigRepository $configRepo)
    {
        $this->configRepo=$configRepo;
    }

    public function index(){


        $allConfig=ConfigRepository::findAll();

        return $this->setCode(200)->setData($allConfig)->send();

    }

}