<?php
/**
 * Created by PhpStorm.
 * User: mallahsoft
 * Date: 08/05/19
 * Time: 08:53 م
 */

namespace Modules\ConfigModule\Http\Controllers\API;


use Modules\CommonModule\Helper\ApiResponseHelper;
use Modules\ConfigModule\Repository\CharityRepository;
use Illuminate\Routing\Controller;

class CharityApiController extends Controller
{
    use ApiResponseHelper;


    private $charityRepo;

    public function __construct(CharityRepository $charityRepo)
    {
        $this->charityRepo = $charityRepo;
    }


    function index()
    {

        $charities = $this->charityRepo->findAll();
        return $this->setData($charities)->setCode(200)->send();
    }


}