<?php

namespace Modules\ConfigModule\Http\Controllers\API;
use Modules\ConfigModule\Repository\SubscriptionRepository;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CommonModule\Helper\ApiResponseHelper;

class SubscriptionApiController extends Controller
{

      use ApiResponseHelper;

      private $subscription;

    public function __construct(SubscriptionRepository $subScrib)
    {
          $this->subscription=$subScrib;
    }


    public function index()
    {
      $sub_data=$this->subscription->findAll();
      return $this->setData($sub_data)->setCode(200)->send();

    }

}
