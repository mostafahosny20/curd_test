<?php

namespace Modules\ConfigModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\ConfigModule\Repository\SubscriptionRepository;

class SubscriptionController extends Controller
{
    private $subscription;

    public function __construct(SubscriptionRepository $subScrib)
    {
          $this->subscription=$subScrib;
    }

    public function index()
    {
      $subscrib_data =   $this->subscription->findAll();
      return view('config::subscribtion.index', ['subscriptions' => $subscrib_data]);
    }

    public function create()
    {
        return view('config::subscribtion.create');
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');
        $this->subscription->save($data);

        return redirect('admin-panel/config-module/subscription')->with('success', 'success');
    }

    public function edit($id)
    {
      $subscrib = $this->subscription->find($id);

      return view('config::subscribtion.edit',['subscription'=>$subscrib]);
    }

    public function update(Request $request, $id)
    {

      $data = $request->except('_token', '_method');

      $subscrib = $this->subscription->find($id);

      $this->subscription->update($data,$subscrib);

      return redirect('admin-panel/config-module/subscription')->with('updated', 'updated');

    }

    public function destroy($id)
    {
      $subscrib = $this->subscription->find($id);
      $this->subscription->delete($subscrib);

      return redirect()->back();
    }
}
