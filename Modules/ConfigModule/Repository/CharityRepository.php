<?php
/**
 * Created by PhpStorm.
 * User: mallahsoft
 * Date: 1/15/19
 * Time: 12:18 PM
 */

namespace Modules\ConfigModule\Repository;


use Modules\ConfigModule\Entities\Charity;

class CharityRepository
{

     function findAll()
    {

        $charity = Charity::all();

        return $charity;
    }


    public function find($id)
    {
      return Charity::find($id);
    }

    public function delete($charity)
    {
        Charity::destroy($charity->id);
    }

    public function update($data, $charity)
    {
        $ncharity=$charity->update($data);
        return $ncharity;
    }

    static function save($data)
    {
      $charity = Charity::create($data);
      return $charity;

    }

}
