<?php

namespace Modules\ConfigModule\Repository;


use Modules\ConfigModule\Entities\Subscription;

class SubscriptionRepository
{


    public function findAll()
    {
      return Subscription::all();
    }


    public function find($id)
    {
      return Subscription::find($id);
    }

    public function delete($subscrib)
    {
      Subscription::destroy($subscrib->id);
    }

    public function update($data, $subscribtion)
    {
        $subscrib=$subscribtion->update($data);
        return $subscrib;
    }

    static function save($data)
    {
      $subscriptions = Subscription::create($data);
      return $subscriptions;

    }
}
