<?php

namespace Modules\ConfigModule\Entities;
use Modules\AreaModule\Entities\City;
use Modules\AreaModule\Entities\Zone;

use Illuminate\Database\Eloquent\Model;

class Charity extends Model
{
    protected $fillable = ['name','phone','city_id','zone_id','address'];

    protected $appends =['city','zone'];


    function getZone(){
        return $this->belongsTo(Zone::class,'zone_id');
    }

    function getZoneAttribute(){
        return $this->getZone;
    }
    function getCity(){
        return $this->belongsTo(City::class,'city_id');
    }

    function getCityAttribute(){
        return $this->getCity;
    }

}
