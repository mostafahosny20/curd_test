@extends('commonmodule::layouts.master')

@section('title')
    {{__('config::charity.charity_title')}}
@endsection

@section('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('assets/admin/bower_components/select2/dist/css/select2.min.css')}}">
@endsection

@section('content-header')
    <section class="content-header">
        <h1> {{__('config::charity.charity_title')}} </h1>

    </section>
@endsection

@section('content')
    <section class="content">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{__('config::charity.charity_title')}}</h3>
            </div>
            @if (count($errors) > 0)
                @foreach ($errors->all() as $item)
                    <p class="alert alert-danger">{{$item}}</p>
            @endforeach
        @endif
        <!-- /.box-header -->
        <form class="form-horizontal" action="{{url('/admin-panel/config-module/charity')}}/{{ $charity->id }}" method="POST">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                <div class="box-body">

                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">

                                @foreach($activeLang as $lang)
                                    <li @if($loop->first) class="active" @endif >
                                        <a href="#{{ $lang->display_lang }}" data-toggle="tab">{{ $lang->display_lang }}</a>
                                    </li>
                                @endforeach

                            </ul>

                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>


                    <div class="form-group">
                        {{-- name --}}
                        <label class="control-label col-sm-2" for="Name">{{__('config::charity.name')}} ({{ $lang->display_lang }}):</label>
                        <div class="col-sm-8">
                            <input type="text" value="{{$charity->name}}" autocomplete="off" class="form-control" placeholder="Write Name" name="name" required>
                        </div>
                    </div>

                    <div class="form-group">
                        {{-- phone --}}
                        <label class="control-label col-sm-2" for="phone">{{__('config::charity.phone')}} ({{ $lang->display_lang }}):</label>
                        <div class="col-sm-8">
                            <input type="text" value="{{$charity->phone}}" autocomplete="off" class="form-control" placeholder="Write Phone" name="phone" required>
                        </div>
                    </div>


                    {{-- city--}}
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="city_id">{{__('areamodule::zone.cityName')}} : </label>
                        <div class="col-sm-8">
                            <select class="form-control" name="city_id" id="city" required>
                                <option value=""> اختر المدينة</option>
                                @foreach($cities as $city)
                                    <option {{($charity->city_id==$city->id)?'selected':''}} value="{{$city->id}}">{{ $city->translate(''.$lang->lang)->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- zone--}}
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="zone_id">{{__('areamodule::zone.name')}} : </label>
                        <div class="col-sm-8">
                            <select class="form-control" name="zone_id" id="zone" required>
                                @foreach($zones as $zone)
                                  <option {{($charity->zone_id==$zone->id)?'selected':''}} value="{{$zone->id}}">{{ $zone->translate(''.$lang->lang)->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        {{-- address --}}
                        <label class="control-label col-sm-2" for="Name">{{__('config::charity.address')}} ({{ $lang->display_lang }}):</label>
                        <div class="col-sm-8">
                            <input type="text" value="{{$charity->address}}" autocomplete="off" class="form-control" placeholder="Write Name" name="address" required>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{url('/admin-panel/config-module/charity')}}" type="button" class="btn btn-default">{{__('config::charity.cancel')}} &nbsp; <i class="fa fa-remove" aria-hidden="true"></i> </a>
                    <button type="submit" class="btn btn-primary pull-right">{{__('config::charity.submit')}} &nbsp; <i class="fa fa-save"></i></button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </section>
    <script src="http://demo.expertphp.in/js/jquery.js"></script>
    <script type="text/javascript">

        $('#city').on('change',function(){
            var cityID = $(this).val();
            if(cityID){
                $.ajax({
                    type:"GET",
                    url:"{{url('admin-panel/getZoneList')}}/"+cityID,
                    success:function(res){
                        if(res){
                            $("#zone").empty();
                            $("#zone").append('<option value="">اختر المنطقة</option>');
                            $.each(res,function(key,value){
                                $("#zone").append('<option value="'+value.id+'">'+value.name+'</option>');
                            });

                        }else{
                            $("#zone").empty();
                        }
                    }
                });
            }else{
                $("#zone").empty();
            }
        });
    </script>
@endsection

@section('javascript')
    <!-- CK Editor -->
    <script src="{{asset('assets/admin/bower_components/ckeditor/ckeditor.js')}}"></script>

    @foreach ($activeLang as $lang)
        <script>
            $(function () {
                CKEDITOR.replace('editor' + {{$lang->id}});
            });

        </script>
    @endforeach

@endsection
