<?php

namespace Modules\UserModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CommonModule\Helper\ApiResponseHelper;
use Modules\CommonModule\Helper\ApiAuthHelper;
use Illuminate\Support\Facades\Auth;
use Modules\UserModule\Repository\UserRepository;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Modules\UserModule\Transformers\UserResource;
use Modules\UserModule\Entities\User;
use Socialite;
class UserAuthController extends Controller
{

  use ApiResponseHelper;
  use ApiAuthHelper;

  public function __construct(UserRepository $userrepo){
    $this->user_repo=$userrepo;
  }

  function redirectToProvider(){
            return Socialite::driver('facebook')->redirect();
  }

  function handleProviderCallback(){
            try {
            $user = Socialite::driver('facebook')->user();
print_r($user);
            } catch (Exception $e) {

            }
            // $authUser = $this->findOrCreateUser($user);
            // auth()->login($authUser, true);
            // return redirect()->to('/');
        }


    function login(Request $request){

      $rules=[
        'email'=>'required|email',
        'password'=>"required|min:6"
      ];
      $validator=$this->validate($request->all(),$rules);

      if($validator->fails()){
        foreach ($validator->errors()->all() as  $message) {
          $message=$this->validation_messages($validator->errors());
          return $this->setCode(400)->setError($message)->send();
        }
      }

      else{
          $credentials = $request->only('email', 'password');
          if (! $token = JWTAuth::attempt($credentials)) {
              return $this->setCode(401)->setError('Unauthorized')->send();
          }

        $user=$this->getAuthUser();
          $userRes=new UserResource($user);
          $data=['token'=>$token,'user'=>$userRes];
       return $this->setCode(200)->setData($data)->send();

    }
  }

  function register (Request $request){


    // return $this->setCode(200)->setData($request->all())->send();


      $rules=[
        'name'=>'required',
        'last_name'=>'required',
        'email'=>'required|email|unique:users',
        'password'=>"required|min:6",
        'con_password'=>'required',
        'phone'=>'required||unique:users|regex:/(01)[0-9]{9}/',
        'birth_date'=>'required',
        'gender'=>'required',
        'government_id'=>'required|exists:governments,id',
        'city_id'=>'required|exists:cities,id',
        'zone_id'=>'required|exists:zones,id',
      ];

      $validator=$this->validate($request->all(),$rules);

      if($validator->fails()){
        $message=$this->validation_messages($validator->errors());
        return $this->setCode(400)->setError($message)->send();
      }
      else{
          $user=$this->user_repo->save($request->all());
          $userRes= new UserResource($user);

            if($token = JWTAuth::fromUser($user)){
              $data=['token'=>$token,'user'=>$userRes];
              return $this->setCode(200)->setData($data)->send();
            }
            else return $this->setCode(400)->setError('there is an error in login')->send();


      }

  }


  public function addresses()
  {
//    $addresses=Auth::user()->address;

    $addresses=User::find(2)->address;
    return $this->setCode(200)->setData($addresses)->send();

  }

  public function checkAuth()
  {
    $token =true;  //JWTAuth::parseToken()->authenticate();
    if($token){
      return $this->setCode(404)->send();
    }
    else
    return $this->setCode(200)->send();

  }

}
