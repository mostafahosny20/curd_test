<?php

namespace Modules\UserModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CommonModule\Helper\ApiResponseHelper;
use Modules\UserModule\Repository\AddressRepository;
use JWTAuth;

class AddressApiController extends Controller
{

  use ApiResponseHelper;

  public $user;
  public $addressRepository;


  public function __construct(AddressRepository $addressRepository)
  {
      $this->user = JWTAuth::parseToken()->authenticate();
      $this->addressRepository = $addressRepository;

  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
      $data = $this->addressRepository->findAllByUser($this->user->id);

      // Return Response
      return $this->setCode(200)->setData($data)->send();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('usermodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
      $addressData=$request->except('_token');

      $addressData['user_id'] = $this->user->id;
      // $data['longitude'=>"",'latitude'=>"",'phone'=>""];

      $data = $this->addressRepository->saveUserAddress($addressData);

      return $this->setCode(200)->setData($data)->send();

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('usermodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('usermodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
