<?php
namespace Modules\UserModule\Http\Controllers;
use Illuminate\Routing\Controller;

use Modules\UserModule\Repository\SocialiteRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\CommonModule\Helper\ApiResponseHelper;

class SocialiteApiController extends Controller
{
    private $socialiterepository;
    use ApiResponseHelper;

    public function __construct()
    {
    $this->socialiterepository=new SocialiteRepository ();

    }
    /**
     * Redirect the user to the social network authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
      $redirect_url=$this->socialiterepository->getRedirectUrlByProvider($provider);
      return $this->setCode(200)->setData($redirect_url)->send();
    }
    /**
     * Obtain the user information from social network
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {

      $result = $this->socialiterepository->loginWithSocialite($provider);
        setcookie('name', json_encode($result), time()+10,'/');

        return redirect('/#/login');
        // ->withCookie(cookie('name',$result['redirect_url'],8));

    }
}
