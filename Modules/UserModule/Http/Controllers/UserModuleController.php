<?php

namespace Modules\UserModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UserModule\Repository\UserRepository;
use Yajra\DataTables\DataTables;
use Modules\UserModule\Entities\User;


class UserModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    protected $userRepository;
    public function __construct(UserRepository $UserRepository){
        $this->userRepository=$UserRepository;
    }
    public function index()
    {
        // $users=$this->userRepository->findAll();
        return view('usermodule::user.index');
    }



    public function getData(){
        //
        return Datatables::of($this->userRepository->findAll())
        ->addColumn('id',function ($user){
            return $user->id ?? '';
        })

        ->addColumn('name',function ($user){
            return $user->name ?? '';
        })

        ->addColumn('last_name',function ($user){
            return $user->last_name ?? '';
        })

        ->addColumn('phone',function ($user){
            return $user->phone ?? '';
        })

        ->addColumn('gender',function ($user){
            return $user->gender ?? '';
        })

        ->addColumn('cityName',function ($user){
            return $user->city->name ?? '';
        })

        ->addColumn('user_status',function ($user){
            return $user->user_status ?? '';
        })

        ->addColumn('created_at',function ($user){
            return $user->created_at ?? '';
        })

        ->addColumn('view',function ($user){
            return '<a href="'.url("usermodule/users/".$user->id).'" type="button" class="btn btn=success"><i class="fa fa-eye" aria-hidden="true"></i></a>';
        })

        // ->addColumn('edit',function ($user){
        //     return '<a href="'.url("usermodule/users/".$user->id).'/edit" type="button" class="btn btn=success"><i class="fa fa-pencil" aria-hidden="true"></i></a>';
        // })
        ->rawColumns(['view' => 'view'])
        ->make(true);

    }



    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('usermodule::user.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $user=$this->userRepository->findUser($id);
        return view('usermodule::user.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('usermodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
