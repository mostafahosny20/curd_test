<?php

namespace Modules\UserModule\Transformers;
// use Modules\AreaModule\Entities\City;
// use Modules\AreaModule\Entities\Zone;
// use Modules\AreaModule\Entities\Government;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=>$this->name,
            'email'=>$this->email,
            'phone'=>$this->phone,
            'birth_date'=>$this->birth_date,
            'gender'=>$this->gender,
            // 'citytets'=>$this->city->name,
            // 'city'=>$this->city->translations[0]->name,
            // 'government'=>$this->government->translations[0]->name,
            // 'zone'=>$this->zone->translations[0]->name,

        ];
    }
}
