<?php
namespace Modules\UserModule\Repository;
use Modules\UserModule\Entities\User;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;
use Modules\CommonModule\Helper\ApiResponseHelper;
use Modules\CommonModule\Helper\ApiAuthHelper;
use Modules\UserModule\Transformers\UserResource;

class SocialiteRepository
{
  use ApiResponseHelper;
  use ApiAuthHelper;

  public function getRedirectUrlByProvider($provider): array
  {

      return [
          'redirectUrl' => Socialite::driver('facebook')
              ->stateless() //      The stateless method may be used to disable session state verification. This is useful when adding social authentication to an API:
              ->redirect()
              ->getTargetUrl()
      ];
  }


  public function loginWithSocialite($provider): array
  {
      $userSocial = Socialite::driver($provider)->stateless()->user();

      if ($this->isSocialPresent($userSocial)) {

          $user = $this->searchUserByEmail($userSocial->email);
          if ($user) {

              $token=$this->doLogin($user);
              if ($token) {
                $userRes=new UserResource($user);
                $data=['token'=>$token,'user'=>$userRes,'code'=>200];
                  // $data = [ 'data' => ['user'=>$user,'token' => $token],];
                  return $data;
              }
              return $this->setCode(401)->setError('Error in register')->send();
          } else {

              $user = $this->createUser($userSocial,'facebook');

              $token=$this->doLogin($user);
              if ($token) {
                $userRes=new UserResource($user);
                $data=['token'=>$token,'user'=>$userRes,'code'=>200];
                  // $data = [ 'data' => ['user'=>$user,'token' => $token],];
                  return $data;
              }
              else {
                return $this->prepareErrorResult();

              }

                  // ? $this->prepareSuccessResult($user)
          }
      } else {


          return $this->prepareErrorResult();

      }
  }


    public static function isSocialPresent($socialiteUser): bool
    {
      return $socialiteUser
          && isset($socialiteUser->email)
          && isset($socialiteUser->id);
    }
    public static function compareUserWithSocialite($user, $socialiteUser)
    {
        return Hash::check(
            $socialiteUser->email . $socialiteUser->id,
            $user->password
        );
    }


    private function searchUserByEmail($email): ?User
    {
        return User::where('email', $email)
            ->first();
    }


    function createUser($userSocial,$provider){
        $user = New User();
        $user->name=$userSocial->name;
        $user->email=$userSocial->email;
        $user->provider_id=$userSocial->id;

        $user->save();

        return $user;
}

}



?>
