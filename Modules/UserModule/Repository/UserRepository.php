<?php
namespace Modules\UserModule\Repository;
use Modules\UserModule\Entities\User;

class UserRepository
{

  function save($register_data){

    $user=User::create($register_data);
    return $user;
  }

  // find all users

  function findAll()
  {
  	$users = User::all();
  	return $users;
  }

  // find user by id
  function findUser($id){
    $user=User::find($id);
    return $user;
  }


}



?>
