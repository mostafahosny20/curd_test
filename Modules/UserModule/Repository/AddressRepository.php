<?php
namespace Modules\UserModule\Repository;

use Modules\UserModule\Entities\UserAddress;

class AddressRepository
{

  function findAllByUser($user_id)
  {
    $address=UserAddress::where('user_id',$user_id)->get();

    // echo "<pre>";
    // print_r($address->first()->zone->name);
    // echo "</pre>";
    //
    // die('done');

    return $address;
  }

  function saveUserAddress ($data){
    return UserAddress::create($data);
  }

}



?>
