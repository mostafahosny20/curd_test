@extends('commonmodule::layouts.master')

@section('title')
    {{__('usermodule::user.pagetitle')}}
@endsection

@section('css')
    <link rel="stylesheet"
          href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content-header')
    <section class="content-header">
        <h1>
            {{__('usermodule::user.pagetitle')}}
        </h1>
    </section>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{__('usermodule::user.pagetitle')}}</h3>
                    {{-- Add New --}}
                    <!-- <a href="{{url('admin-panel/package/create')}}" type="button" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; {{__('usermodule::user.addnew')}}</a> -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="userIndex" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{trans ('usermodule::user.id')}}</th>
                                <th>{{trans ('usermodule::user.name')}}</th>
                                <th>{{trans ('usermodule::user.last_name')}}</th>
                                <th>{{trans ('usermodule::user.phone')}}</th>
                                <th>{{trans ('usermodule::user.gender')}}</th>
                                <th>{{trans ('usermodule::user.city')}}</th>
                                <th>{{trans ('usermodule::user.status')}} </th>
                                <th>{{trans ('usermodule::user.created_at')}}</th>
                                <!-- <th>{{__('usermodule::user.lastLog')}}</th> -->
                                <th>{{trans ('usermodule::user.view')}}</th>
                                <!-- <th>{{trans ('usermodule::user.edit')}}</th> -->
                            </tr>
                            </thead>
                            <tbody>


                            </tbody>

                        </table>
                    </div>
                {{--{{ $users->links() }}--}}

                <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('javascript')


    @include('commonmodule::includes.swal')

    <!-- DataTables -->
    <script src="{{asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script>
        $(function() {

        });
        $(document).ready(function () {

            $('#userIndex').DataTable({
                dom: 'lBfrtip',
                buttons: [
                    {extend: 'print', text: 'ﻃﺒﺎﻋﻪ', messageBottom: ' <strong>ﺟﻤﻴﻊ اﻟﺤﻘﻮﻕ ﻣﺤﻔﻮﻇﺔ  Makdak .</strong>'},
                    {extend: 'excel', text: ' اﻛﺴﻴﻞ'},
                ],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                processing: true,
                serverSide: true,
                autoWidth: false,
                order: [[0, 'desc']],
                language: {
                    search : "  ﺑﺤﺚ :  ",
                    paginate: {
                        previous: "اﻟﺴﺎﺑﻖ",
                        next: "اﻟﺘﺎﻟﻰ"
                    },
                    info:           "ﻋﺮﺽ _START_ اﻟﻲ _END_ ﻣﻦ _TOTAL_ ﻣﻦ اﻟﺼﻔﻮﻑ",
                    lengthMenu:     "ﻋﺮﺽ _MENU_ ﻣﻦ اﻟﺼﻔﻮﻑ",
                    loadingRecords: "ﺟﺎﺭﻱ اﻟﺘﺤﻤﻴﻞ...",
                    processing:     "ﺟﺎﺭﻱ اﻟﺘﺤﻤﻴﻞ...",
                    zeroRecords:    "ﻻ ﻳﻮﺟﺪ ﻧﺘﺎﺋﺞ",
                    infoEmpty:      "0 ﻋﺮﺽ to 0 of 0 ﻣﻦ اﻟﺼﻔﻮﻑ",
                    infoFiltered:   "(ﻋﺮﺽ ﻣﻦ _MAX_ ﺻﻒ)",
                } ,
                ajax: '{{ route('getData') }}',
                columns: [
                    { data: 'id', name: 'id' },

                    { data: 'name', name: 'name' },

                    { data: 'last_name', name: 'last_name' },

                    { data: 'phone', name: 'phone' },

                    { data: 'gender', name: 'gender' },

                    { data: 'cityName', name: 'cityName' },

                    { data: 'user_status', name: 'user_status' },

                    { data: 'created_at', name: 'created_at' },

                    // { data: 'lastLog', name: 'lastLog' , orderable: false, searchable: false},

                    { data: 'view', name: 'view', orderable: false, searchable: false },

                    // { data: 'edit', name: 'edit' , orderable: false, searchable: false},



                ]

            });

        })

    </script>

@endsection
