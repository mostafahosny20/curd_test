<?php

return array(
    'id'=>'الرقم',
    'name'=>'الإسم',
    'last_name'=>'الإسم الأخير',
    'phone'=>'الهاتف',
    'gender' => 'النوع',
    'city'=>'المدينه',
    'status'=>'الحاله',
    'created_at'=> 'وقت التسجيل',
    'view'=>'عرض',
    'edit'=>'تعديل',
    'pagetitle'=>'المستخدمين',
    'mail'=>'الإيميل',
    'wallet'=>'المحفظه',
    'wallet_expired'=>'صلاحيه المحفظه',
    'package'=>'السله',
    'packages'=>'السلات',
    'products'=>'المنتجات',
    'discount'=>'خصم',
    'tax'=>'ضريبه',
    'ship_price'=>'خدمه التوصيل',
    'title'=>'اسم السله',

);
