<?php

namespace Modules\UserModule\Entities;

use Modules\UserModule\Entities\UserAddress;
use Modules\AreaModule\Entities\Government;
use Modules\AreaModule\Entities\City;
use Modules\AreaModule\Entities\Zone;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements JWTSubject
{
  use Notifiable;


  protected $fillable = [
      'name',
      'last_name',
      'email',
      'phone',
      'password',
      'birth_date',
      'gender',
      'government_id',
      'city_id',
      'zone_id',

  ];


  public function setPasswordAttribute($value)
  {
      $this->attributes['password'] = Hash::make($value);
  }

  protected $hidden = [
      'password', 'remember_token',
  ];

      /**
       * Get the identifier that will be stored in the subject claim of the JWT.
       *
       * @return mixed
       */
      public function getJWTIdentifier()
      {
          return $this->getKey();
      }

          /**
       * Return a key value array, containing any custom claims to be added to the JWT.
       *
       * @return array
       */
      public function getJWTCustomClaims()
      {
          return [];
      }



    function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    function zone()
    {
        return $this->belongsTo(Zone::class, 'zone_id', 'id');
    }

    function government()
    {
        return $this->belongsTo(Government::class, 'government_id', 'id');
    }

    function address()
    {
        return $this->hasMany(UserAddress::class);
    }
    function orders()
    {
      return $this->hasMany(Order::class);
    }

}
