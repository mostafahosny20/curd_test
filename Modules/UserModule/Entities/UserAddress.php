<?php

namespace Modules\UserModule\Entities;
use Modules\AreaModule\Entities\City;
use Modules\AreaModule\Entities\Government;
use Modules\AreaModule\Entities\Zone;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
  protected $fillable = [
      'longitude', 'latitude', 'phone', 'address', 'flat_num',
      'building_num', 'floor_num', 'other', 'user_id', 'city_id',
      'zone_id', 'government_id'
  ];
  protected $hidden=['getGovernment','getCity','getZone'];
  protected $appends =['government','city','zone'];


        function getGovernment(){
          return $this->belongsTo(Government::class,'government_id');
        }

        function getCity(){
          return $this->belongsTo(City::class,'city_id');
        }

        function getZone(){
          return $this->belongsTo(Zone::class,'city_id');
        }

        function user()
        {
            return $this->belongsTo(User::class);
        }

        function getZoneAttribute(){
            return $this->getZone;
        }

        function getCityAttribute(){
            return $this->getCity;
        }

        function getGovernmentAttribute(){
            return $this->getGovernment;
        }


}
