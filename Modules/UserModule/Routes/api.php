<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/usermodule', function (Request $request) {
    return $request->user();
});
Route::post('user/login', 'UserAuthController@login');
Route::post('user/register', 'UserAuthController@register');

Route::post('user/auth', 'UserAuthController@checkAuth');

Route::get('/social/{provider}', 'SocialiteApiController@redirectToProvider');

// Route::post('user/address', 'UserAuthController@addresses');

Route::resource('user/address', 'AddressApiController');



// Route::name('getData')->get('users','UserApiController@index');