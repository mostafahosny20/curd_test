<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('usermodule')->group(function() {
    Route::get('/users', 'UserModuleController@index');
    Route::get('/users/{id}', 'UserModuleController@show');
    Route::get('/usersData','UserModuleController@getData')->name('getData');
});

// Route::get('social/auth/redirect/facebook', 'UserAuthController@redirectToProvider')->name('socialite');

// Route::get('facebook/callback', 'UserAuthController@handleProviderCallback');
Route::get('/{provider}/callback', 'SocialiteApiController@handleProviderCallback');
