<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('assets/admin/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p> ﻟﻮﺣﻪ اﻟﺘﺤﻜﻢ </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header"> اﻟﻘﺎﺋﻤﻪ اﻟﺮﺋﻴﺴﻴﺔ</li>

            <!-- Common Module -->
            <li>
                <a href="{{url('/admin-panel')}}">
                    <i class="fa fa-home"></i> <span>اﻟﺼﻔﺤﻪ اﻟﺮﺋﻴﺴﻴﻪ </span>
                    <span class="pull-right-container">
                </span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-stack-exchange" aria-hidden="true"></i>
                    <span>اﻟﻤﺨﺰﻥ</span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <!-- Stock -->
                    <li><a href="{{ url('admin-panel/store/stock') }}"><i
                                    class="fa fa-circle-o"></i> اﻟﻤﺨﺰﻭﻥ</a></li>

                    <!-- Reciete -->
                    <li><a href="{{ url('/admin-panel/store/receipt') }}"><i
                                    class="fa fa-circle-o"></i>اﻹﻳﺼﺎﻻﺕ</a></li>

                </ul>
            </li>

            {{-- Excel upload --}}
            {{--<li class="treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-file-excel-o" aria-hidden="true"></i> <span>اﻟﺘﻘﺎﺭﻳﺮ</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<!-- products -->--}}
                    {{--<li><a href="{{url('/admin-panel/excel-upload')}}"><i--}}
                                    {{--class="fa fa-circle-o"></i>ﺗﻘﺮﻳﺮ اﻟﻤﻨﺘﺠﺎﺕ</a></li>--}}

                    {{--<!-- packages -->--}}
                    {{--<li><a href="{{ url('admin-panel/package/excel-upload') }}"><i--}}
                                    {{--class="fa fa-circle-o"></i>ﺗﻘﺮﻳﺮ اﻟﺼﻔﻘﺎﺕ</a></li>--}}

                {{--</ul>--}}
            {{--</li>--}}

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-qrcode" aria-hidden="true"></i> <span>اﻟﻤﻨﺘﺠﺎﺕ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!-- Categories -->
                    <li><a href="{{ url('productmodule/product-categories') }}"><i
                                    class="fa fa-circle-o"></i>الاقسام</a></li>

                    <!-- Products -->
                    <li><a href="{{ url('productmodule/product') }}"><i
                                    class="fa fa-circle-o"></i>اﻟﻤﻨﺘﺠﺎﺕ</a>
                    </li>
                    <li>
                        <a href="{{url('/productmodule/edit-product-price')}}">
                            <i class="fa fa-money"></i> <span>ﺗﻌﺪﻳﻞ اﻟﻤﻨﺘﺠﺎﺕ </span>
                            <span class="pull-right-container">
                            </span>
                        </a>
                    </li>



                </ul>
            </li>







            <li class="treeview">
                <a href="#">
                    <i class="fa fa-qrcode" aria-hidden="true"></i> <span>اﻟﻄﻠﺒﺎﺕ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!-- Current -->
                    <li><a href="{{ url('admin-panel/orders/current') }}"><i
                                    class="fa fa-circle-o"></i>اﻟﻄﻠﺒﺎﺕ اﻟﺤﺎﻟﻴﻪ</a></li>

                    <!-- Done -->
                    <li><a href="{{ url('admin-panel/orders/done') }}"><i
                                    class="fa fa-circle-o"></i>اﻟﻄﻠﺒﺎﺕ اﻟﻤﻨﺘﻬﻴﻪ</a></li>

                    <!-- Canceled -->
                    <li><a href="{{ url('admin-panel/orders/cancel') }}"><i
                                    class="fa fa-circle-o"></i> اﻟﻄﻠﺒﺎﺕ اﻟﻤﻠﻐﻴﻪ</a></li>

                </ul>
            </li>



            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user" aria-hidden="true"></i><span>اﻟﻤﺴﺘﺨﺪﻣﻴﻦ </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('usermodule/users') }}"><i
                                    class="fa fa-circle-o"></i>اﻟﻤﺴﺘﺨﺪﻣﻴﻦ</a></li>

                    <li><a href="{{ url('admin-panel/all-users-page/notifications') }}"><i
                                    class="fa fa-circle-o"></i> ﺇﺭﺳﺎﻝ ﺇﺷﻌﺎﺭاﺕ</a></li>


                </ul>
            </li>







            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                    <span>اﻟﺘﻘﺎﺭﻳﺮ </span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="{{url('/admin-panel/order-product-report')}}">
                            <i class="fa fa-money"></i> <span>ﺗﻘﺎﺭﻳﺮ اﻟﻤﻨﺘﺠﺎﺕ </span>
                            <span class="pull-right-container">
                             </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/admin-panel/user-report')}}">
                            <i class="fa fa-money"></i> <span>ﺗﻘﺎﺭﻳﺮ اﻟﻤﺴﺘﺨﺪﻣﻴﻦ</span>
                            <span class="pull-right-container">
                             </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/admin-panel/orders-report')}}">
                            <i class="fa fa-money"></i> <span>ﺗﻘﺎﺭﻳﺮ اﻟﻄﻠﺒﺎﺕ</span>
                            <span class="pull-right-container">
                             </span>
                        </a>
                    </li>
                </ul>
            </li>



            <li class="treeview">
                <a href="">
                    <i class="fa fa-cog" aria-hidden="true"></i><span>اﻹﻋﺪاﺩاﺕ </span>
                    <span class="pull-right-container">
                                     <i class="fa fa-angle-left pull-right"></i>

                </span>
                </a>
                <ul class="treeview-menu">

                    <li>
                        <a href="{{ url('admin-panel/config-module') }}">
                            <i class="fa fa-cog" aria-hidden="true"></i><span>اﻹﻋﺪاﺩاﺕ </span><span class="pull-right-container"></span>
                        </a>
                    </li>



                    <li><a href="{{ url('admin-panel/voucher') }}"><i
                                    class="fa fa-map"></i> الكوبونات</a></li>

                     <li><a href="{{ url('admin-panel/order-status-types') }}"><i
                                                class="fa fa-map"></i> ﺣﺎﻟﻪ اﻟﻄﻠﺐ</a></li>


                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-truck" aria-hidden="true"></i>
                    <span>اﻟﺸﺤﻦ </span>
                    <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                </a>
                <ul class="treeview-menu">
                    <!-- Companies -->
                    <!-- <li><a href="{{ url('admin-panel/shipping/company') }}"><i
                                    class="fa fa-circle-o"></i>اﻟﺸﺮﻛﺎﺕ</a></li> -->

                    <!-- Drivers -->
                    <li><a href="{{ url('admin-panel/shipping/driver') }}"><i
                                    class="fa fa-circle-o"></i> اﻟﺴﺎﺋﻘﻴﻦ</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-globe" aria-hidden="true"></i> <span>اﻟﻤﻨﺎﻃﻖ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!-- Country -->
                    <li><a href="{{ url('admin-panel/country') }}"><i
                                    class="fa fa-circle-o"></i>اﻟﺪﻭﻝ</a></li>

                    <!-- Government -->
                    <li><a href="{{ url('admin-panel/government') }}"><i
                                    class="fa fa-circle-o"></i> اﻟﻤﺤﺎﻓﻈﺎﺕ</a></li>

                    <!-- city -->
                    <li><a href="{{ url('admin-panel/city') }}"><i
                                    class="fa fa-circle-o"></i>اﻟﻤﺪﻥ</a></li>
                    <!-- zone -->
                    <li><a href="{{ url('admin-panel/zone') }}"><i
                                    class="fa fa-circle-o"></i> اﻟﻤﻨﺎﻃﻖ</a></li>
                </ul>
            </li>




            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user" aria-hidden="true"></i> <span>اﻟﻤﺪﻳﺮﻳﻦ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!-- admins -->
                    <li><a href="{{ url('admin-panel/admins') }}"><i
                                    class="fa fa-circle-o"></i>اﻟﻤﺪﻳﺮﻳﻦ</a></li>

                    <!-- roles -->
                    <li><a href="{{ url('admin-panel/roles') }}"><i
                                    class="fa fa-circle-o"></i> اﻟﺼﻼﺣﻴﺎﺕ</a></li>


                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
