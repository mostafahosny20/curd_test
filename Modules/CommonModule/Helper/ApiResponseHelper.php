<?php
/**
 * Created by PhpStorm.
 * User: mallahsoft
 * Date: 14/01/18
 * Time: 09:25 م
 */

namespace Modules\CommonModule\Helper;

use Response;
use Validator;

trait ApiResponseHelper
{

    /**
     * @var Request
     */
    protected $request;


    /**
     * @var array
     */
    protected $body;



    /**
     * Set response data.
     *
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->body['data'] = $data;
        return $this;
    }



    public function setError($error)
    {
        $this->body['status'] = 'error';
        $this->body['message'] = $error;
        return $this;
    }

    public function setSuccess($message)
    {
        $this->body['status'] = 'success';
        $this->body['message'] = $message;
        return $this;
    }

    public function setCode($code)
    {
        $this->body['code'] = $code;
        return $this;
    }


    public function send()
    {
        //return $this->response->json();
        //return response()->json();
        return response()->json($this->body);

    }



        private function makeAuthenticationCookie($result)
        {
            $result['cookie'] = cookie('authentication',
                json_encode($result),
                8000,
                null,
                null,
                false,
                false
            );
            return $result;
        }

    public function prepareErrorResult(): array
    {
        return $this->makeAuthenticationCookie([
            'error' => 'هذا الاكونت غير موجود تأكد من انه يحتوى على ايميل !',
            'redirect' => '/login',
            'redirect_url' => '/#/',
            'code'=>401
        ]);
    }

    public function prepareSuccessResult(User $user): array
    {
        return $this->makeAuthenticationCookie([
            'api_token' => $user->api_token,
            'user_id' => $user->id,
            'redirect_url' => '/#/'
        ]);
    }


    public function sendCollection($collection,$code)
    {
        //return $this->response->json();
        //return response()->json();
        return response()->json($collection,200);

    }

    public function validate($inputs , $rules){
        $validator = Validator::make($inputs, $rules);

        return $validator;
    }

    public function validation_messages($errors){
      foreach ($errors->all() as  $message) {

        if ($errors->has('email')) return $message;
        elseif ($errors->has('password')) return $message;
        elseif ($errors->has('name')) return $message;
        elseif ($errors->has('last_name')) return $message;
        elseif ($errors->has('phone')) return $message;
        elseif ($errors->has('password')) return $message;
        elseif ($errors->has('con_password')) return $message;
        elseif ($errors->has('gender')) return $message;
        elseif ($errors->has('government_id')) return $message;
        elseif ($errors->has('city_id')) return $message;
        elseif ($errors->has('zone_id')) return $message;
        else return $message;
    }
    }

//    private function initialize()
//    {
//        $body = [
//            'data' => [],
//        ];
//
//        $this->body = $body;
//    }


}
