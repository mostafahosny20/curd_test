<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/areamodule', function (Request $request) {
    return $request->user();
});

Route::get('governments','Api\GovernmentApiController@index');
Route::get('cities/{id}','Api\CityApiController@getCities');
Route::get('zones/{id}','Api\ZoneApiController@getZones');
