<?php

namespace Modules\AreaModule\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\AreaModule\Repository\GovernmentRepository;
use Modules\CommonModule\Helper\ApiResponseHelper;


class GovernmentApiController extends Controller
{
  use ApiResponseHelper;
    private $governmentsRepository;
    public function __construct(GovernmentRepository $governmentsrepo){

      $this->governmentsRepository=$governmentsrepo;

    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
      $governments=$this->governmentsRepository->findAllGovernments();
      return $this->setCode(200)->setData($governments)->send();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('areamodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('areamodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('areamodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
