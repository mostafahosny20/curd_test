<?php

namespace Modules\AreaModule\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AreaModule\Repository\CityRepository;
use Modules\CommonModule\Helper\ApiResponseHelper;

class CityApiController extends Controller
{

  use ApiResponseHelper;

  public function __construct(CityRepository $cityrepo){

    $this->cityreop=$cityrepo;

  }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $this->cityreop->citytest();

        // return view('areamodule::index');
    }

    public function getCities($id)
    {
        $cities=$this->cityreop->findCitiesByGovernment($id);
        return $this->setCode(200)->setData($cities)->send();
    }


}
