<?php

namespace Modules\AreaModule\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AreaModule\Repository\ZoneRepository;
use Modules\CommonModule\Helper\ApiResponseHelper;

class ZoneApiController extends Controller
{

  use ApiResponseHelper;

  private $zoneRepository;
  public function __construct(ZoneRepository $zonerepo){

    $this->zoneRepository=$zonerepo;

  }

  public function getZones($id)
  {
      $zones=$this->zoneRepository->findZonesByCityId($id);
      return $this->setCode(200)->setData($zones)->send();
  }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('areamodule::index');
    }




    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('areamodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('areamodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('areamodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
