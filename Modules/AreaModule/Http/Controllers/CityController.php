<?php

namespace Modules\AreaModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AreaModule\Repository\GovernmentRepository;
use Modules\AreaModule\Repository\CityRepository;

class CityController extends Controller
{

  private $cityReop;

  public function __construct(CityRepository $city,GovernmentRepository $government){

    $this->cityReop=$city;
    $this->governmentReop=$government;

  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
      $city=$this->cityReop->findAllCities();
        return view('areamodule::City.index',compact('city'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
      $governments=$this->governmentReop->findAllGovernments();
      $country =$this->governmentReop->findAllcountries();

        return view('areamodule::City.create',compact('country'));

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('areamodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
      $city=$this->cityReop->findCity($id);
      $governments=$this->governmentReop->findAllGovernments();

        return view('areamodule::City.edit',compact('city','governments'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
