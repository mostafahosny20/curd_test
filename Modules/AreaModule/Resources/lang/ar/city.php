<?php

return array(
//government
    'citytitle' => 'المدن',
    'homepage' => 'الرئيسية',
    'addnew' => 'أضف جديد',
    'id' => 'الرقم',
    'operation' => 'العمليات',
    'name' => 'المدينة',
    'counrtyName' => 'الدوله',
    'governmentName' => 'المحافظه',
    'customer' => 'العميل',
    'total' => 'الاجمالى',
    'status' =>  'الحاله',
    'phone_code '=> 'رقم الدوله',
    'date' =>  'التاريخ',
    'view'=> 'عرض',
    'delete'=> 'حذف',
    'code'=> 'كود الدوله',
    'photo'=> 'الصوره',
    'mt' => 'معلومات العنوان',
    'md' => 'معلومات الوصف',
    'tag' => 'ذات صلة',
    'cancel' => 'الغاء',
    'submit' => 'حفظ',
);
