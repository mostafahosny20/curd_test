@extends('commonmodule::layouts.master')

@section('title')
    {{__('areamodule::country.countrytitle')}}
@endsection

@section('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('assets/admin/bower_components/select2/dist/css/select2.min.css')}}">
@endsection

@section('content-header')
    <section class="content-header">
        <h1> {{__('areamodule::country.countrytitle')}} </h1>

    </section>
@endsection

@section('content')
    <section class="content">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{__('areamodule::country.countrytitle')}}</h3>
            </div>
            @if (count($errors) > 0)
                @foreach ($errors->all() as $item)
                    <p class="alert alert-danger">{{$item}}</p>
            @endforeach
        @endif
        <!-- /.box-header -->
            <form class="form-horizontal" action="{{url('admin-panel/country')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="box-body">

                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">

                                @foreach($activeLang as $lang)
                                    <li @if($loop->first) class="active" @endif >
                                        <a href="#{{ $lang->display_lang }}" data-toggle="tab">{{ $lang->display_lang }}</a>
                                    </li>
                                @endforeach

                            </ul>

                            <div class="tab-content">
                                @foreach($activeLang as $lang)

                                    <div class="tab-pane @if($loop->first) active @endif" id="{{ $lang->display_lang }}">
                                        <div class="form-group">
                                            {{-- name --}}
                                            <label class="control-label col-sm-2" for="Name">{{__('areamodule::country.name')}} ({{ $lang->display_lang }}):</label>
                                            <div class="col-sm-8">
                                                <input type="text" autocomplete="off" class="form-control" placeholder="Write Name" name="{{$lang->lang}}[name]" required>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>

                    {{-- Upload photo --}}
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="img">{{__('areamodule::country.photo')}}:</label>
                        <div class="col-sm-8">
                            <input type="file" autocomplete="off" name="photo">
                        </div>
                    </div>

                    {{-- phone_code --}}
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="phone_code">{{__('areamodule::country.phone_code')}}: </label>
                        <div class="col-sm-8">
                            <input type="text" autocomplete="off" class="form-control" placeholder="Write Phone Code" name="phone_code" required>
                        </div>
                    </div>

                    {{-- code --}}
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="code">{{__('areamodule::country.code')}} : </label>
                        <div class="col-sm-8">
                            <input type="text" autocomplete="off" class="form-control" placeholder="Write code" name="code" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="box-header">
                            <pre><h4>SEO Columns : </h4></pre>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                @foreach($activeLang as $lang)
                                    <li @if($loop->first) class="active" @endif >
                                        <a href="#seo{{ $lang->display_lang }}" data-toggle="tab">{{ $lang->display_lang }}</a>
                                    </li>
                                @endforeach
                            </ul>


                            <div class="tab-content">
                                @foreach($activeLang as $lang)
                                    <div class="tab-pane @if($loop->first) active @endif" id="seo{{ $lang->display_lang }}">
                                        <div class="form-group">
                                            {{-- Meta Title --}}
                                            <label class="control-label col-sm-2" for="title">{{trans('areamodule::country.mt')}} ({{ $lang->display_lang }}):</label>
                                            <div class="col-sm-8">
                                                <input type="text" autocomplete="off" class="form-control" placeholder="Write information about your title" name="{{ $lang->lang}}[meta_title]">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            {{-- Meta Description --}}
                                            <label class="control-label col-sm-2" for="title">{{trans('areamodule::country.md')}} ({{ $lang->display_lang }}):</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control" placeholder="Write information about your category" name="{{ $lang->lang}}[meta_desc]" cols="15" rows="10"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            {{-- Meta Keywords --}}
                                            <label class="control-label col-sm-2" for="title">{{trans('areamodule::country.tag')}} ({{ $lang->display_lang }}):</label>
                                            <div class="col-sm-8">
                                                <input type="text" autocomplete="off" class="form-control" placeholder="Enter Tags" name="{{ $lang->lang }}[meta_keywords]">
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- /.nav-tabs-custom -->
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{url('/admin-panel/country')}}" type="button" class="btn btn-default">{{__('areamodule::country.cancel')}} &nbsp; <i class="fa fa-remove" aria-hidden="true"></i> </a>
                    <button type="submit" class="btn btn-primary pull-right">{{__('areamodule::country.submit')}} &nbsp; <i class="fa fa-save"></i></button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </section>
@endsection

@section('javascript')
    <!-- CK Editor -->
    <script src="{{asset('assets/admin/bower_components/ckeditor/ckeditor.js')}}"></script>

    @foreach ($activeLang as $lang)
        <script>
            $(function () {
                CKEDITOR.replace('editor' + {{$lang->id}});
            });

        </script>
    @endforeach

@endsection
