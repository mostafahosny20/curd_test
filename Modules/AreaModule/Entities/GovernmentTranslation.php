<?php

namespace Modules\AreaModule\Entities;

use Illuminate\Database\Eloquent\Model;

class GovernmentTranslation extends Model
{
  protected $table = 'governments_translations';

  protected $fillable = ['name'];
  public $timestamps = false;

}
