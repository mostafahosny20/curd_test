<?php

namespace Modules\AreaModule\Entities;


use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\AreaModule\Entities\ZoneTranslation;

class Zone extends Model
{
  use Translatable;
  protected $fillable = ['country_id','government_id','city_id'];

  protected $table = "zones";
  public $translatedAttributes = ['name'];
  public $translationModel = ZoneTranslation::class;


    function government(){
     return $this->belongsTo(Government::class,'government_id');
   }

    function country(){
     return $this->belongsTo(Country::class,'country_id');
   }

   function city(){
    return $this->belongsTo(City::class,'city_id');
  }

}
