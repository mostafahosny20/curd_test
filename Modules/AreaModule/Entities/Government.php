<?php

namespace Modules\AreaModule\Entities;


use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\AreaModule\Entities\GovernmentTranslation;


class Government extends Model

{
  use Translatable;
  protected $fillable = ['country_id'];

  protected $table = "governments";
  public $translatedAttributes = ['name'];
  public $translationModel = GovernmentTranslation::class;

public function country(){
  return $this->belongsTo(Country::Class,'country_id');
}

}
