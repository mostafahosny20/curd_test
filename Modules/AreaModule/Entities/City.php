<?php

namespace Modules\AreaModule\Entities;

use Illuminate\Database\Eloquent\Model;

use Dimsav\Translatable\Translatable;

class City extends Model
{
    use Translatable;
  protected $fillable = ['government_id','country_id'];

  protected $table = "cities";
  public $translatedAttributes = ['name'];
  public $translationModel = CityTranslation::class;

 function government(){
  return $this->belongsTo(Government::class,'government_id');
}

 function country(){
  return $this->belongsTo(Country::class,'country_id');
}

}
