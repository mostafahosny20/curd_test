<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('countries', function (Blueprint $table) {
          $table->increments('id');
          $table->string('phone_code')->nullable();
          $table->string('code')->nullable();
          $table->string('photo')->nullable();
          $table->timestamps();
      });

      Schema::create('countries_translations', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->integer('country_id')->unsigned();
          $table->string('locale')->index();
          $table->unique(['country_id', 'locale']);
          $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
          $table->timestamps();
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
