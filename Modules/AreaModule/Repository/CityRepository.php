<?php

namespace Modules\AreaModule\Repository;

use Modules\AreaModule\Entities\City;

class CityRepository{

   function findAllCities()
  {

    $cities=City::all();
    return $cities;

  }
  function findCity($id)
  {
    $city=City::find($id);
    return $city;

  }

  function findCitiesByGovernment($id){

    $cities=City::where('government_id',$id)->get();
    return $cities;
  }





}







?>
