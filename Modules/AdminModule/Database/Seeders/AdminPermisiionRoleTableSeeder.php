<?php

namespace Modules\AdminModule\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\AdminModule\Entities\Admin;
use Spatie\Permission\Models\Role;

class AdminPermisiionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        Role::create(['guard_name' => 'admin', 'name' => 'super_admin']);
        Role::create(['guard_name' => 'admin', 'name' => 'admin']);
        Role::create(['guard_name' => 'admin', 'name' => 'operation']);
        Role::create(['guard_name' => 'admin', 'name' => 'customer_service']);
        Role::create(['guard_name' => 'admin', 'name' => 'shipment']);

        $super_admin = Admin::create([
            'name' => "super_admin",
            'username' => "super_admin",
            'email' => 'super_admin@admin.com',
            'password' => bcrypt('super_admin'),
        ]);

        $operation = Admin::create([
            'name' => "operation",
            'username' => "operation",
            'email' => 'operation@admin.com',
            'password' => bcrypt('operation'),
        ]);

        $customer_service = Admin::create([
            'name' => "customer_service",
            'username' => "customer_service",
            'email' => 'customer_service@admin.com',
            'password' => bcrypt('customer_service'),
        ]);

        $shipment = Admin::create([
            'name' => "shipment",
            'username' => "shipment",
            'email' => 'shipment@admin.com',
            'password' => bcrypt('shipment'),
        ]);

        $super_admin->assignRole('super_admin');
        $operation->assignRole('operation');
        $customer_service->assignRole('customer_service');
        $shipment->assignRole('shipment');


    }
}
