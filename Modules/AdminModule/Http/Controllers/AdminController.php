<?php

namespace Modules\AdminModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Modules\AdminModule\Repository\AdminRepository;
use Modules\OrderModule\Repository\OrderRepository;
use Modules\UserModule\Repository\UserRepository;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{

    public $adminRepo;
    /**
     * @var OrderRepository
     */
    public $orderRepository;

    public $userRepository;

    public function __construct(AdminRepository $adminRepository,OrderRepository $orderRepository,UserRepository $userRepository)
    {
        $this->middleware('auth:admin');
        $this->adminRepo = $adminRepository;
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
    }

    function dashboard()
    {



        $all = $this->orderRepository->allOrdersCount();
        $new = $this->orderRepository->allNewOrdersCount();
        $preparing = $this->orderRepository->allPreparingOrdersCount();
        $prepared = $this->orderRepository->allPreparedOrdersCount();
        $shipping = $this->orderRepository->allShippingOrdersCount();
        $shipped = $this->orderRepository->allShippedOrdersCount();
        $done = $this->orderRepository->allDoneOrdersCount();
        $cancelled = $this->orderRepository->allCancelledOrdersCount();
        $clients = $this->userRepository->findAll();
        $current_orders = $this->orderRepository->current();
        $client_android = $clients->where('platform','=','android')->count();
        $client_ios = $clients->where('platform','=','Ios')->count();
        return view('adminmodule::admins.dashboard',compact(
            'all','new','preparing','prepared','shipping',
            'shipped','done','cancelled','current_orders','client_ios','client_android')
        );
    }

    function index()
    {

        $admins=$this->adminRepo->findAll();
        return view('adminmodule::admins.index',compact('admins'));
    }



    function create()
    {
        $roles = Role::all();

        return view('adminmodule::admins.create',compact('roles'));
    }

    function store(Request $request)
    {

        $data = $request->except('_token', 'role');
        $data['role'] = $request->role;
        $role = $data['role'];
        $this->adminRepo->save($data, $role);

        return redirect('admin-panel/admins')->with('success','Successfully add');
    }

    function edit($id){

        $admin=$this->adminRepo->findById($id);
        $roles = Role::all();


        return view('adminmodule::admins.edit',compact('admin','roles'));
    }

    function update(Request $request,$id){

        $data = $request->except('_token', 'role');
        $role = $request->role;
        $this->adminRepo->update($data,$id, $role);

        return redirect('admin-panel/admins')->with('success','Successfully Edit');

    }

    public function destroy($id)
    {
        $admin = $this->adminRepo->findById($id);
        $this->adminRepo->delete($admin);

        return redirect()->back();
    }
}
