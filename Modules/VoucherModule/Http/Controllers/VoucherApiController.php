<?php

namespace Modules\VoucherModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CommonModule\Helper\ApiResponseHelper;
use Modules\VoucherModule\Repository\VoucherRepository;
use JWTAuth;

class VoucherApiController extends Controller
{

  private $voucherRepository;
  public function __construct(VoucherRepository $repo){
    $this->voucherRepository=$repo;

  }

  use ApiResponseHelper;

  public function checkCode(Request $request){


    $code=$this->voucherRepository->findCode($request->coupon_code);
    if($code){
            $data=$this->calAmount($request->total,$code);
            return $this->setCode(200)->setData($data)->send();
    }
    return $this->setCode(400)->SetError('الكود غير صحيح')->send();

  }


  public function calAmount($total,$code){

      if ($code['amount'] == null)
          $discount = $total * $code['percentage'] / 100;
      else
          $discount = $code['amount'];

      $data = [
          'code' => $code['code'],
          'amount' => $discount,
          'is_free_ship' => $code['is_shipping']
      ];


  return $data;
  }



    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('vouchermodule::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('vouchermodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('vouchermodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('vouchermodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
