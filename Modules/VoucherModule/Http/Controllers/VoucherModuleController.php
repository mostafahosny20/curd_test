<?php

namespace Modules\VoucherModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\VoucherModule\Repository\VoucherRepository;

class VoucherModuleController extends Controller
{

  private $voucherRepository;
  public function __construct(VoucherRepository $repo){
    $this->voucherRepository=$repo;

  }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
      $vouchers =$this->voucherRepository->allVouchers();
        return view('vouchermodule::voucher.index',compact('vouchers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('vouchermodule::voucher.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
      $voucherData=$request->except('_token');
      $this->voucherRepository->saveVoucher($voucherData);

      return redirect('admin-panel/voucher');

        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('vouchermodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
      $voucher=$this->voucherRepository->findVoucherById($id);

        return view('vouchermodule::voucher.edit',compact('voucher'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        $voucherData=$request->except('_token','_method','type');
        $this->voucherRepository->updateVoucher($id,$voucherData);

        return redirect('admin-panel/voucher');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
