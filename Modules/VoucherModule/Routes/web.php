<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('vouchermodule')->group(function() {
    Route::get('/', 'VoucherModuleController@index');
});

Route::group(['prefix' => 'admin-panel', 'middleware' => ['auth:admin']], function() {
  Route::resource('/voucher', 'VoucherModuleController');

});
