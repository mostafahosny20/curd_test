<?php
/**
 * Created by PhpStorm.
 * User: hossam
 * Date: 1/25/19
 * Time: 5:09 PM
 */

namespace Modules\VoucherModule\Repository;

use Modules\VoucherModule\Entities\Voucher;

class VoucherRepository
{


    function allVouchers()
    {
      return  Voucher::all();
    }




    function saveVoucher($data)
    {
    return Voucher::create($data);
    }

    function updateVoucher($id,$voucherData)
    {
    return Voucher::where('id',$id)->update($voucherData);
    }


    function findVoucherById($id)
    {
    return Voucher::find($id);
    }

    // use BaseHelper;

    function findCode($code){
      $now=date('Y-m-d');
        $item = Voucher::where('code',$code)
                          ->where('status','enabled')
                          // ->where('min_total','<=',$total)
                          ->where('from','<=',$now)
                          ->where('to','>=',$now)
                          ->first();
        return $item;


    }




}
