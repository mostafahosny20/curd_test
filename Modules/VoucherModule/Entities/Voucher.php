<?php

namespace Modules\VoucherModule\Entities;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $fillable = ['code','from','to','amount','percentage',
                          'status','min_total','is_shipping','max_num_of_use','num_of_use','voucher_type'];
}
