@extends('commonmodule::layouts.master')

@section('title')
  {{__('vouchermodule::voucher.pagetitle')}}
@endsection

@section('css')
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('assets/admin/bower_components/select2/dist/css/select2.min.css')}}">

{{-- Metro CSS --}}
<link rel="stylesheet" href="{{asset('assets/admin/treeview.css')}}">
@endsection

@section('content-header')
<section class="content-header">
  <h1> {{__('vouchermodule::voucher.pagetitle')}} </h1>

</section>
@endsection

@section('content')
<section class="content">
  <!-- Horizontal Form -->
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">{{__('vouchermodule::voucher.pagetitle')}}</h3>
    </div>
    @if (count($errors) > 0)
      @foreach ($errors->all() as $item)
        <p class="alert alert-danger">{{$item}}</p>
      @endforeach
    @endif
    <!-- /.box-header -->
    <form class="form-horizontal" action="{{url('/admin-panel/voucher')}}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}

      <div class="box-body">
        {{-- Upload photo --}}
        <div class="form-group">
          <label class="control-label col-sm-2" for="code">{{__('vouchermodule::voucher.code')}} :</label>
          <div class="col-sm-3">
            <input data-validation="required" type="text" id="code"  required class="col-sm-3 form-control"   autocomplete="off" name="code">
          </div>



          <label class="control-label col-sm-2" for="code">{{__('vouchermodule::voucher.max_of_use')}} :</label>
          <div class="col-sm-3">
            <input data-validation="required" type="number" id="max_num_of_use" required class="col-sm-3 form-control"   autocomplete="off" name="max_num_of_use">
          </div>

        </div>


          <div class="form-group">
            <label class="control-label col-sm-2" for="code">{{__('vouchermodule::voucher.min_total')}} :</label>
            <div class="col-sm-8">
              <input data-validation="required" type="number" required id="min_total"  class="col-sm-3 form-control"   autocomplete="off" name="min_total">
            </div>

          </div>





        <div class="form-group">
          <label class="control-label col-sm-2">{{__('vouchermodule::voucher.from')}}  : </label>
          <div class="col-sm-3">
            <input type="date" autocomplete="off" class="form-control"  name="from" required>
          </div>

          <label class="control-label col-sm-2">{{__('vouchermodule::voucher.to')}}  : </label>
          <div class="col-sm-3">
            <input type="date" autocomplete="off" class="form-control"  name="to" required>
          </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="type">{{__('vouchermodule::voucher.dicount_type')}} : </label>
            <div class="col-sm-3">
                <select class="form-control" name="type" id="type" required>
                    <option value="1">{{__('vouchermodule::voucher.amount')}}</option>
                    <option value="2">{{__('vouchermodule::voucher.percentage')}}</option>
                </select>
            </div>

            <label class="control-label col-sm-2" id="type_title"> </label>
            <div class="col-sm-3" id="type_val">

            </div>

        </div>



        



        <div class="form-group">
          <label class="control-label col-sm-2" for="img">{{__('vouchermodule::voucher.status')}}  :</label>

          <div class="col-lg-3">
            <label><input type="radio" name="status" value="enabled" checked > {{__('vouchermodule::voucher.enabled')}}</label>
            <label><input type="radio" name="status" value="disabled">  {{__('vouchermodule::voucher.disabled')}}</label>
          </div>


          <label class="control-label col-sm-2" for="img">{{__('vouchermodule::voucher.shipping')}}  :</label>

          <div class="col-lg-3">
            <label><input type="radio" name="is_shipping" value="1"  > {{__('vouchermodule::voucher.unshiped')}}</label>
            <label><input type="radio" name="is_shipping" value="0" checked>  {{__('vouchermodule::voucher.shiped')}}</label>
          </div>
        </div>

        </div>


        <!-- /.box-body -->
      <div class="box-footer">
        <a href="{{url('/admin-panel/voucher')}}" type="button" class="btn btn-default">{{__('vouchermodule::voucher.cancel')}} &nbsp; <i class="fa fa-remove" aria-hidden="true"></i> </a>
        <button type="submit" class="btn btn-primary pull-right">{{__('vouchermodule::voucher.submit')}} &nbsp; <i class="fa fa-save"></i></button>
      </div>
      <!-- /.box-footer -->
    </form>
  </div>
</section>
@endsection

@section('javascript')


<script src="http://demo.expertphp.in/js/jquery.js"></script>
<script type="text/javascript">
    $('#type').change(function () {
        var type = $(this).val();

        if (type==1) {
          $('#type_title').text('المبلغ');

          $('#type_val').html('<input type="text" required autocomplete="off" class="form-control"  name="amount">');

        }
        else{
          $('#type_title').text('النسبة');
          $('#type_val').html('<input type="text" required autocomplete="off" class="form-control"  name="percentage">');


            }
        });
</script>



  {{-- Treeview --}}
<script  src="{{asset('assets/admin/metro.js')}}" > </script>

<!-- CK Editor -->
<script src="{{asset('assets/admin/bower_components/ckeditor/ckeditor.js')}}"></script>

<!-- Select2 -->
<script src="{{asset('assets/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

<script>
  //Initialize Select2 Elements
  $('.select2').select2();
</script>

@foreach ($activeLang as $lang)
<script>
  $(function () {
    CKEDITOR.replace('editor' + "{{$lang->id}}");
  });
</script>
<script>

  function randomCode() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 40; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }
  $('#auto_generate').click(function () {
     $('#code').val(randomCode());
  })
</script>
@endforeach

@endsection
