@extends('commonmodule::layouts.master')

@section('title')
  {{__('vouchermodule::voucher.pagetitle')}}
@endsection

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
 @endsection

@section('content-header')
  <section class="content-header">
    <h1>
      {{__('vouchermodule::voucher.pagetitle')}}
    </h1>
  </section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{{__('vouchermodule::voucher.pagetitle')}}</h3>
          {{-- Add New --}}
          <a href="{{url('admin-panel/voucher/create')}}" type="button" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; {{__('vouchermodule::voucher.addnew')}}</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="productIndex" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>{{__('vouchermodule::voucher.id')}}</th>
                <th>{{__('vouchermodule::voucher.code')}}</th>
                  <th>{{__('vouchermodule::voucher.from')}}</th>
                  <th>{{__('vouchermodule::voucher.to')}}</th>
                  <th>{{__('vouchermodule::voucher.amount')}}</th>
                  <th>{{__('vouchermodule::voucher.percentage')}}</th>
                  <th>{{__('vouchermodule::voucher.status')}}</th>
                  <th>{{__('vouchermodule::voucher.shipping')}}</th>
                  <th>{{__('vouchermodule::voucher.max_of_use')}}</th>
                  <th>{{__('vouchermodule::voucher.num_of_use')}}</th>
                  <th>{{__('vouchermodule::voucher.min_total')}}</th>
                <th>{{__('vouchermodule::voucher.operation')}}</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($vouchers as $item)
              <tr>
                <td> {{$item->id}} </td>

                <td> {{$item->code}} </td>

                  <td>{{$item->from}}</td>
                  <td>{{$item->to}}</td>
                  <td>{{($item->amount)?$item->amount:__('vouchermodule::voucher.none')}}</td>
                  <td>{{($item->percentage)?$item->percentage:__('vouchermodule::voucher.none')}}</td>
                  <td>
                    @if($item->status == 'enabled')
                      {{__('vouchermodule::voucher.enabled')}}
                    @else
                      {{__('vouchermodule::voucher.disabled')}}
                    @endif
                  </td>

                  <td>
                    @if($item->is_shipping == 1)
                      {{__('vouchermodule::voucher.unshiped')}}
                    @else
                      {{__('vouchermodule::voucher.shiped')}}
                    @endif
                  </td>
                  <td>{{$item->max_num_of_use}}</td>
                  <td>{{$item->num_of_use}}</td>
                  <td>{{$item->min_total}}</td>


                <td> {{-- Edit --}}
                  <a title="Edit" href="{{url('admin-panel/voucher/' . $item->id . '/edit')}}" type="button" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                  {{-- Delete --}}
                  <form class="inline" action="{{url('admin-panel/voucher/' . $item->id)}}" method="POST">
                    {{ method_field('DELETE') }} {!! csrf_field() !!}
                    <!-- <button title="Delete" type="submit" onclick="return confirm('Are you sure, You want to delete this Data?')" type="button"
                      class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button> -->
                  </form>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
@endsection

@section('javascript')


  @include('commonmodule::includes.swal')

  <!-- DataTables -->
  <script src="{{asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

  <script>
      $(document).ready(function () {
          $('#productIndex').DataTable({
              'paging'      : true,
              'lengthChange': true,
              'searching'   : true,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
          });
      })

  </script>

@endsection
