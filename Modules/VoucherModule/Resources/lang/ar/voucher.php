<?php

return array(

  'pagetitle' => 'الكوبونات',
  'homepage' => 'الرئيسية',
  'addnew' => 'أضف جديد',

  'id' => 'الترتيب',

  'voucher' => 'الكوبونات',
  'operation' => 'العمليات',
  'code' => 'الكود',
  'from' => 'تاريخ البدأ',
  'to'=>'تاريخ الانتهاء',
    'enabled'=>'مفعل',
    'disabled'=>' غير مفعل',
  'amount' => 'المبلغ',
  'status' => 'الحالة',
  'percentage' => 'النسبة',
  'cancel' => 'الغاء',
  'submit' => 'حفظ',
  'shipping'=>'الشحن',
  'shiped'=>'مدفوع',
  'unshiped'=>'مجانى',
  'none'=>'لا يوجد',
  'dicount_type'=>'نوع الخصم',
  'max_of_use'=>'اقصى عدد الاستخدام',
  'num_of_use'=>'عدد الاستخدام',
  'min_total'=>"اقل قيمة للسلة"



);
