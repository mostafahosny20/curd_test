<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> New Order Placed Mail </title>
</head>
<body>

<h1>You got New Order Placed</h1>

<h3> User Name : {{ $order->user->name }}</h3>
<h3> Sub total : {{ $order->sub_total }}</h3>
<h3> Total     : {{ $order->total }}</h3>


<h2> Order Packages </h2>

@foreach($order->orderDetail as $detail)
    <li>
        <b> اسم السله </b>
        <p>{{$detail->package_name}}</p>
    </li>
    <li>
        <b> كميه السله </b>
        <p>{{$detail->package_quantity}}</p>
    </li>
    <li>
        <b> كوبون الخصم </b>
        <p>{{$detail->coupon_code}}</p>
    </li>

    <li style="height: 60px;">
        <b> معاد التوصيل </b>
        <p>{{$detail->delivery_date}}</p>
    </li>
    <li>
        <b> وقت التوصيل </b>
        <p> {{ $detail->delivery_time }} </p>
    </li>
@endforeach


<h2> Order Address </h2>

@foreach($order->userAddresses as $address)
    <li>
        <b>المدينة </b>
        <p>{{$address->city->name }}</p>
    </li>

    <li>
        <b>الحى </b>
        <p>{{$address->zone->name }}</p>
    </li>
    <li>
        <b>رقم التلفون </b>
        <p>{{$address->phone }}</p>
    </li>
    <li>
        <b> رقم الشقه </b>
        <p>{{$address->flat_num}}</p>
    </li>
    <li>
        <b> رقم المبنى </b>
        <p>{{$address->building_num}}</p>
    </li>
    <li>
        <b> رقم الطابق </b>
        <p>{{$address->floor_num}}</p>
    </li>
    <li>
        <b> وصف </b>
        <p>{{$address->other}}</p>
    </li>
@endforeach


</body>
</html>