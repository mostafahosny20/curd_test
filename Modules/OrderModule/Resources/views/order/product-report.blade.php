@extends('commonmodule::layouts.master')

@section('title')
  {{__('ordermodule::order.pagetitle')}}
@endsection

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
 @endsection

@section('content-header')
  <section class="content-header">
    <h1>
      {{__('ordermodule::order.pagetitle')}}
    </h1>
  </section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{{__('ordermodule::order.pagetitle')}}</h3>
          {{-- Add New --}}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="myTable" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>{{__('ordermodule::order.product_id')}}</th>
                <th>{{__('ordermodule::order.product')}}</th>
                <th>{{__('ordermodule::order.quantity')}}</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($orders as $item)
                    <tr>
                        <td>{{ $item->product_id }}</td>
                        <td>{{ $item->product->title }}</td>
                        <td>{{ $item->quantity }}</td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
@endsection

