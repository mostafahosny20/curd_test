@extends('commonmodule::layouts.master')

@section('title')
{{trans('ordermodule::order.pagetitle')}}
@endsection

@section('css')

@endsection

@section('content-header')
<section class="content-header">
    <h1>
        طباعة الطلب
    </h1>
</section>
@endsection

@section('content')


<section class="content invoice">
<!-- title row -->
<div class="row">
    <div class="col-xs-12">
        <h2 class="page-header">
            <i class="fa fa-globe"></i> مقضاك.
            <small class="pull-right">  التاريخ: {{$order->created_at}} </small>
        </h2>
    </div>
    <!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
        من
        <address>
            <strong>مقضاك.</strong><br>
{{--            <br>--}}
{{--            الهاتف: <br>--}}
{{--            البريد الالكترونى:--}}
        </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
        الى
        <address>
            <strong>{{$order->user->name}}</strong><br>
{{--                @if(isset($order->user->zone))--}}
{{--                    {{$order->user->zone->translation}},<br>--}}
{{--                @endif--}}
{{--            @if(isset($order->user->city))--}}

            {{$order->address}}<br>
{{--            @endif--}}
{{--                @if(isset($order->user->government))--}}

{{--                {{$order->user}}<br>--}}
{{--                @endif--}}

            @if(isset($order->user->phone))

            الهاتف:  {{$order->user->phone}}<br>
            @endif
            @if(isset($order->user->email))

            البريد الالكترونى: {{$order->user->email}}
             @endif
        </address>
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
        <b>رقم الاوردر:</b> {{$order->id}}<br>
        <b>حالة الدفع:</b> {{$order->payment_status}}<br>
{{--        <b>طريقة الدفع:</b> {{$order->orderDetail->payment_type}}<br>--}}
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- Table row -->
<div class="row">
    <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>الاسم</th>
                <th>السعر</th>
                <th>الكميه</th>
                <th>الاجمالى</th>
            </tr>
            </thead>
            <tbody>
            @foreach($order->orderProducts as $product)
                <tr>
                    <td>{{$product->product->title }}</td>
                    <td>{{round($product->product->sell_price,2) }}</td>
                    <td>{{$product->quantity }}</td>
                    <td>{{round($product->total_price,2) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
    <!-- accepted payments column -->
{{--    <div class="col-xs-6">--}}
{{--        <p class="lead">Payment Methods:</p>--}}
{{--        <img src="../../dist/img/credit/visa.png" alt="Visa">--}}
{{--        <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">--}}
{{--        <img src="../../dist/img/credit/american-express.png" alt="American Express">--}}
{{--        <img src="../../dist/img/credit/paypal2.png" alt="Paypal">--}}

{{--        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">--}}
{{--            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg--}}
{{--            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.--}}
{{--        </p>--}}
{{--    </div>--}}
    <!-- /.col -->
    <div class="col-xs-6">
{{--        <p class="lead">Amount Due 2/22/2014</p>--}}

        <div class="table-responsive">
            <table class="table">
                <tbody><tr>
                    <th style="width:50%">المجموع:</th>
                    <td>{{round($order->sub_total,2)}}</td>
                </tr>
                <tr>
                    <th>ضرائب </th>
                    <td>{{round($order->tax,2)}}</td>
                </tr>
                <tr>
                    <th>شحن:</th>
                    <td>{{round($order->shipping,2)}}</td>
                </tr>
                <tr>
                    <th>خصم:</th>
                    <td>{{$order->discount}}</td>
                </tr>
                <tr>
                    <th>المجموع الكلى:</th>
                    <td>{{round($order->total,2)}}</td>
                </tr>
                </tbody></table>
        </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<!-- this row will not appear when printing -->
<div class="row no-print">
    <div class="col-xs-12">
        <button onclick="window.print();"  class="btn btn-default"><i class="fa fa-print"></i> طباعه</button>
{{--        <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment--}}
{{--        </button>--}}
{{--        <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">--}}
{{--            <i class="fa fa-download"></i> Generate PDF--}}
{{--        </button>--}}
    </div>
</div>
</section>

@endsection

@section('javascript')

@include('commonmodule::includes.swal')
@include('ordermodule::includes.update_status_js')
@include('ordermodule::includes.createOrderShipping')

@endsection
