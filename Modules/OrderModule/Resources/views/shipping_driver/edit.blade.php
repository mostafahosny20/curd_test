@extends('commonmodule::layouts.master')

@section('title')
  السائقين
@endsection

@section('content-header')
<section class="content-header">
    <h1>بيانات السائق</h1>
</section>
@endsection

@section('content')
<section class="content">
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">بيانات السائق</h3>
        </div>
        @if (count($errors) > 0)
            @foreach ($errors->all() as $item)
                <p class="alert alert-danger">{{$item}}</p>
            @endforeach
        @endif
    <!-- /.box-header -->
        <form class="form-horizontal" action="{{url('/admin-panel/shipping/driver')}}/{{ $driver->id }}" method="POST">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            <div class="box-body">
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            @foreach($activeLang as $lang)
                                <li @if($loop->first) class="active" @endif >
                                    <a href="#{{ $lang->display_lang }}"
                                        data-toggle="tab">{{ $lang->display_lang }}</a>
                                </li>
                            @endforeach
                        </ul>

                        <div class="tab-content">
                        @foreach($activeLang as $lang)

                            <div class="tab-pane @if($loop->first) active @endif" id="{{ $lang->display_lang }}">
                                <div class="form-group">
                                    {{-- title --}}
                                    <label class="control-label col-sm-2" for="name">اسم السائق
                                        ({{ $lang->display_lang }}):</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="{{ $driver->name }}" autocomplete="off" class="form-control" name="name" required>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                            <div class="form-group">
                                {{-- phone --}}
                                <label class="control-label col-sm-2" for="phone">هاتف السائق
                                    ({{ $lang->display_lang }}):</label>
                                <div class="col-sm-8">
                                    <input type="text" value="{{ $driver->phone }}" autocomplete="off" class="form-control" name="phone">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="company" class="control-label col-sm-2">اختر شركة</label>
                                <div class="col-sm-6">
                                    <select name="company_id" class="form-control">
                                        @foreach ($companies as $company)
                                            <option @if ($ids == $company->id) selected @endif value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <a href="{{url('/admin-panel/shipping/driver')}}" type="button"
                    class="btn btn-default">الغاء &nbsp; <i class="fa fa-remove" aria-hidden="true"></i>
                </a>
                <button type="submit" class="btn btn-primary pull-right">حفظ
                    &nbsp; <i class="fa fa-save"></i></button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
</section>
@endsection
