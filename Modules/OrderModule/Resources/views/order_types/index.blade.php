@extends('commonmodule::layouts.master')

@section('title')
  {{__('ordermodule::orderTypes.title')}}
@endsection

@section('css')
  <link rel="stylesheet" href="{{ asset('assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
 @endsection

@section('content-header')
  <section class="content-header">
    <h1>
      {{__('ordermodule::orderTypes.title')}}
    </h1>
  </section>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">{{__('ordermodule::orderTypes.title')}}</h3>
          {{-- Add New --}}
          <!-- <a href="{{url('admin-panel/order-status-types/create')}}" type="button" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; {{__('packagemodule::packageSlider.addnew')}}</a> -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="packageIndex" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>{{__('ordermodule::orderTypes.id')}}</th>
                <th>{{__('ordermodule::orderTypes.status')}}</th>
                <th>{{__('ordermodule::orderTypes.type')}}</th>
                <th>النسبة</th>
{{--                <th>{{__('ordermodule::orderTypes.operation')}}</th>--}}
              </tr>
            </thead>
            <tbody>
              @foreach ($statuses as $status)
              <tr>
                <td> {{$status->id}} </td>

                <td> {{$status->title}} </td>
                <td> {{$status->status_type->type}} </td>
                <td> {{$status->percentage}} </td>

{{--                --}}
{{--                <td> --}}{{-- Edit --}}
{{--                  <a title="Edit" href="{{url('admin-panel/order-status-types/' . $status->id . '/edit')}}" type="button" class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></a>--}}
{{--                  --}}{{-- Delete --}}
{{--                  <form class="inline" action="{{url('admin-panel/order-status-types/' . $status->id)}}" method="POST">--}}
{{--                    {{ method_field('DELETE') }} {!! csrf_field() !!}--}}
{{--                    <button title="Delete" type="submit" onclick="return confirm('Are you sure, You want to delete this Data?')" type="button"--}}
{{--                      class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>--}}
{{--                  </form>--}}
{{--                </td>--}}
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
@endsection

@section('javascript')


  @include('commonmodule::includes.swal')

  <!-- DataTables -->
  <script src="{{asset('assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

  <script>
      $(document).ready(function () {
          $('#packageIndex').DataTable({
              'paging'      : true,
              'lengthChange': true,
              'searching'   : true,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
          });
      })

  </script>

@endsection
