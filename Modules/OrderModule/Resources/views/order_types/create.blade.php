@extends('commonmodule::layouts.master')

@section('title')
    {{__('ordermodule::orderTypes.title')}}
@endsection

@section('css')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('assets/admin/bower_components/select2/dist/css/select2.min.css')}}">

@endsection

@section('content-header')
    <section class="content-header">
        <h1> {{__('ordermodule::orderTypes.title')}} </h1>

    </section>
@endsection

@section('content')
    <section class="content">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">{{__('ordermodule::orderTypes.title')}}</h3>
            </div>
            @if (count($errors) > 0)
                @foreach ($errors->all() as $item)
                    <p class="alert alert-danger">{{$item}}</p>
                @endforeach
            @endif
        <!-- /.box-header -->
            <form class="form-horizontal" action="{{url('/admin-panel/order-status-types')}}" method="POST"
                  enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="box-body">

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title"><strong>{{trans('ordermodule::orderTypes.status')}}</strong>
                            :</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="title">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title"><strong>{{trans('ordermodule::orderTypes.type')}}</strong>
                            :</label>
                            <div class="col-sm-3">
                            <select class="form-control" name="status_type_id">
                            <option> {{trans('ordermodule::orderTypes.type_select')}} </option>
                                @foreach($types as $type)

                                        <option value="{{ $type->id }}">{{ $type->type }}</option>

                                @endforeach
                            </select>
                        </div>
                    </div>



                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="{{url('/admin-panel/order-status-types')}}" type="button"
                       class="btn btn-default">{{__('ordermodule::orderTypes.cancel')}} &nbsp; <i class="fa fa-remove" aria-hidden="true"></i>
                    </a>
                    <button type="submit" class="btn btn-primary pull-right">{{__('ordermodule::orderTypes.submit')}}
                        &nbsp; <i class="fa fa-save"></i></button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </section>
@endsection

@section('javascript')

    {{-- Treeview --}}
{{--    <script src="{{asset('assets/admin/treeview.js')}}"></script>--}}

    <!-- CK Editor -->
{{--    <script src="{{asset('assets/admin/bower_components/ckeditor/ckeditor.js')}}"></script>--}}

    <!-- Select2 -->
    <script src="{{asset('assets/admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>

    <script>
        //Initialize Select2 Elements
        $('.select2').select2();
    </script>


@endsection
