@extends('commonmodule::layouts.master')

@section('title')
  الشحن
@endsection

@section('content-header')
<section class="content-header">
    <h1>بيانات شركة الشحن</h1>
</section>
@endsection

@section('content')
<section class="content">
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">شركة شحن جديدة</h3>
            {{-- Add New Driver --}}
            <a href="{{url('admin-panel/shipping/driver/create')}}" type="button" class="btn btn-success pull-right">
                <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; أضف سائق جديد</a>
        </div>
        @if (count($errors) > 0)
            @foreach ($errors->all() as $item)
                <p class="alert alert-danger">{{$item}}</p>
            @endforeach
        @endif
    <!-- /.box-header -->
        <form class="form-horizontal" action="{{url('/admin-panel/shipping/company')}}/{{ $company->id }}" method="POST">
            {{ method_field('put') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            @foreach($activeLang as $lang)
                                <li @if($loop->first) class="active" @endif >
                                    <a href="#{{ $lang->display_lang }}"
                                        data-toggle="tab">{{ $lang->display_lang }}</a>
                                </li>
                            @endforeach
                        </ul>

                        <div class="tab-content">
                        @foreach($activeLang as $lang)

                            <div class="tab-pane @if($loop->first) active @endif" id="{{ $lang->display_lang }}">
                                <div class="form-group">
                                    {{-- title --}}
                                    <label class="control-label col-sm-2" for="name">اسم الشركة
                                        ({{ $lang->display_lang }}):</label>
                                    <div class="col-sm-8">
                                        <input type="text" value="{{ $company->name }}" autocomplete="off" class="form-control" name="name" required>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                            <div class="form-group">
                                {{-- email --}}
                                <label class="control-label col-sm-2" for="email">بريد الشركة
                                    ({{ $lang->display_lang }}):</label>
                                <div class="col-sm-8">
                                    <input type="text" value="{{ $company->email }}" autocomplete="off" class="form-control" name="email">
                                </div>
                            </div>

                            <div class="form-group">
                                {{-- phone --}}
                                <label class="control-label col-sm-2" for="phone">هاتف الشركة
                                    ({{ $lang->display_lang }}):</label>
                                <div class="col-sm-8">
                                    <input type="text" value="{{ $company->phone }}" autocomplete="off" class="form-control" name="phone">
                                </div>
                            </div>

                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <a href="{{url('/admin-panel/shipping/company')}}" type="button"
                    class="btn btn-default">الغاء &nbsp; <i class="fa fa-remove" aria-hidden="true"></i>
                </a>
                <button type="submit" class="btn btn-primary pull-right">حفظ
                    &nbsp; <i class="fa fa-save"></i></button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
</section>
@endsection
