<script src="{{asset('assets/admin/plugins/sweetalert/sweetalert.min.js')}}"></script>

<script src="https://js.pusher.com/4.4/pusher.min.js"></script>

<script>

    $(document).ready(function() {
        $(document).on('click', '#shipping', function(event) {
            event.preventDefault();
            $status  = $('#order_id').val();

            $.ajax({
                type: 'post',
                url: '{{url("admin-panel/order/shipping")}}',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'driver_id': $('#selected_driver').val(),
                    'order_id': $('#order_id').val(),
                },
                success: function(data) {
                    swal("رائع", "تم ارسال طلب الشحن بنجاح", "success", { button: "Ok", });
                    location.reload();
                },
                error:function (error) {
                    console.log(error);
                }

            });

        });




        $(document).on('click', '#start_driver', function(event) {



          if (navigator.geolocation)
          {
            navigator.geolocation.getCurrentPosition(successFunction, errorFunction);


          }
          else
          {
              alert('It seems like Geolocation, which is required for this page, is not enabled in your browser.');
          }
           // setInterval(function(){ alert("Hello"); }, 3000);
            // event.preventDefault();
            // $status  = $('#order_id').val();


        });

    });


    function successFunction(position)
      {
          var lat = position.coords.latitude;
          var long = position.coords.longitude;

          $.ajax({
              type: 'post',
              url: '{{url("fire2")}}',
              data: {
                  '_token': '{{csrf_token()}}',
                  'lat': lat,
                  'lng': long,
              },
              success: function(data) {
                alert('done');
                  // swal("رائع", "تم ارسال طلب الشحن بنجاح", "success", { button: "Ok", });
                  // location.reload();
              },
              error:function (error) {
                alert('fail');

                  console.log(error);
              }

          });


          // alert('Your latitude is :'+lat+' and longitude is '+long);
      }
      function errorFunction(position)
      {
          alert('Error!');
      }
</script>
