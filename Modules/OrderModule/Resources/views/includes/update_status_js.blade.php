<script>

$(document).ready(function() {
    $(document).on('click', '#update-status', function(event) {
        event.preventDefault();
        $status  = $('#order_id').val();

        $.ajax({
            type: 'post',
            url: '{{url("admin-panel/order/update-status")}}',
            data: {
                '_token': $('input[name=_token]').val(),
                'status_id': $('#selected_status').val(),
                'comment': $('#com').val(),
                'order_id': $('#order_id').val(),
            },
            success: function(data) {

              location.reload();
                // console.log(data);
                // let  comment = "";
                // if (data.data.status_comment){
                //     comment = data.data.status_comment;
                // }
                // var row='<tr> <td>'+data.data.status.title+'</td> <td>'+data.data.created_at+'</td><td></td><td>'+comment+'</td></tr>';
                //
                // $(".added_status table tr:first").after(row);
            },
            error:function (error) {
                console.log(error);
            }
        });

    });
});

</script>
