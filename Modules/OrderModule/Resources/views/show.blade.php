@extends('commonmodule::layouts.master')

@section('title')
    {{trans('ordermodule::order.pagetitle')}}
@endsection

@section('css')

@endsection

<link rel="stylesheet" href="{{ asset('assets/admin/dist/css/selection.css')}}">

@section('content-header')
    <section class="content-header">
        <div class="text-center">
          <h4 style="font-weight: 700;">
      الحالة : {{$order->currentStatus->title}}
          </h4 >


        </div>

        <h1>
            تفاصيل الطلب
        </h1>


    </section>
@endsection

@section('content')

    <input type="hidden" id="order_id" value="{{$order->id}}">
    {{ csrf_field() }}
    <section class="content">

        <div class="row">
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center">بيانات الطلب </h3>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b> رقم الطلب</b>
                                <p class="pull-right ">{{ $order->id }}</p>
                            </li>
                            <li class="list-group-item">
                                <b> تاريخ الطلب </b>
                                <p class="pull-right">{{ $order->created_at }}</p>
                            </li>
                            <li class="list-group-item">
                                <b> الاشتراك </b>
                                <p class="pull-right">{{ (isset($order->subscription))?$order->subscription->name:'لا يوجد' }}</p>
                            </li>

                            <li class="list-group-item">
                                <b> المجموع الجزئي </b> <a class="pull-right">{{ $order->sub_total }}</a>
                            </li>
                            <li class="list-group-item">
                                <b> الخصم </b> <a class="pull-right">{{ $order->discount }}</a>
                            </li>
                            <li class="list-group-item">
                                <b> الضرائب </b> <a class="pull-right">{{ $order->tax }}</a>
                            </li>
                            <li class="list-group-item">
                                <b> المحفظه </b> <a class="pull-right">{{ $order->wallet }}</a>
                            </li>
                            <li class="list-group-item">
                                <b> المجموع النهائي </b> <a class="pull-right">{{ $order->total }}</a>
                            </li>
                        </ul>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center"> بيانات العميل </h3>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b> اسم العميل </b>
                                <p class="pull-right ">{{ $order->user->name }}</p>
                            </li>

                            <li class="list-group-item">
                                <b> رقم التلفون </b>
                                <p class="pull-right">{{ $order->user->phone }}</p>
                            </li>

                        </ul>

                    </div>
                    <!-- /.box-body -->
                </div>



                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center"> الجمعية الخيرية </h3>

                        <ul class="list-group list-group-unbordered">
                          @if($order->charity!=null)
                                <li class="list-group-item">
                                    <b> اسم الجمعية </b>
                                    <p class="pull-right ">{{ $order->charity->name}}</p>
                                </li>

                                <li class="list-group-item">
                                    <b> رقم التلفون </b>
                                    <p class="pull-right">{{ $order->charity->phone }}</p>
                                </li>

                                <li class="list-group-item">
                                    <b> المدينة  </b>
                                    <p class="pull-right ">{{ $order->charity->city->name}}</p>
                                </li>

                                <li class="list-group-item">
                                    <b> المنطقة  </b>
                                    <p class="pull-right">{{ $order->charity->zone->name }}</p>
                                </li>


                            @else
                                <li class="list-group-item text-center">
                                    <b >لا يــــوجـــــــد </b>
                                </li>
                            @endif
                        </ul>

                    </div>
                    <!-- /.box-body -->
                </div>


            </div>
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center">تفاصيل السله</h3>

                        <ul class="list-group list-group-unbordered">
                            @foreach($order->orderDetail as $detail)
                                <li class="list-group-item">
                                    <b> اسم السله </b>
                                    <p class="pull-right ">{{$detail->package_name}}</p>
                                </li>
                                <li class="list-group-item">
                                    <b> كميه السله </b>
                                    <p class="pull-right ">{{$detail->package_quantity}}</p>
                                </li>
                                <li class="list-group-item">
                                    <b> كوبون الخصم </b>
                                    <p class="pull-right ">{{$detail->coupon_code}}</p>
                                </li>

                                <li class="list-group-item" style="height: 60px;">
                                    <b> معاد التوصيل </b>
                                    <p class="pull-right ">{{$detail->delivery_date}}</p>
                                </li>
                                <li class="list-group-item">
                                    <b> وقت التوصيل </b>
                                    <p class="pull-right "> {{ $detail->delivery_time }} </p>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer ">
                        <button type="button" id="update-time" class="btn btn-info pull-left" data-toggle="modal"
                                data-target="#exampleModal">
                            تغيير الوقت والتاريخ
                            <i class="fa fa-edit"></i>
                        </button>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">تحديث عنوان التوصيل</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                                <label  class="col-form-label">الوقت:</label>
                                                <select class="form-control" id="delivery_time">

                                                    <option @if($detail->delivery_time=='مساء') selected @endif > مساء</option>
                                                    <option @if($detail->delivery_time=='مساء') selected @endif > مساء</option>

                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label  class="col-form-label">التاريخ:</label>
                                                <input value="{{$detail->delivery_date}}" class="form-control" id="delivery_date" >
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق
                                        </button>
                                        <button type="button" id="update-date-time" class="btn btn-primary">تحديث</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h3 class="profile-username text-center"> عنوان التوصيل </h3>

                        <ul class="list-group list-group-unbordered">
                            @foreach($order->userAddresses as $address)
                                <li class="list-group-item">
                                    <b>المدينة </b>
                                    <p class="pull-right text-bold">{{$address->city->name }}</p>
                                </li>

                                <li class="list-group-item">
                                    <b>الحى </b>
                                    <p class="pull-right text-bold">{{$address->zone->name }}</p>
                                </li>
                                <li class="list-group-item">
                                    <b>رقم التلفون  </b>
                                    <p class="pull-right text-bold">{{$address->phone }}</p>
                                </li>
                                <li class="list-group-item">
                                    <b> رقم الشقه </b>
                                    <p class="pull-right text-bold">{{$address->flat_num}}</p>
                                </li>
                                <li class="list-group-item">
                                    <b> رقم المبنى </b>
                                    <p class="pull-right text-bold">{{$address->building_num}}</p>
                                </li>
                                <li class="list-group-item">
                                    <b> رقم الطابق </b>
                                    <p class="pull-right text-bold">{{$address->floor_num}}</p>
                                </li>
                                <li class="list-group-item">
                                    <b> وصف </b>
                                    <p class="pull-right text-bold">{{$address->other}}</p>
                                </li>
                            @endforeach
                        </ul>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <br>


        <div class="row">
            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border" style="background: blanchedalmond;">

                      <div class="row">
                          <div class="col-lg-3">

                                                    <h3 class="box-title">
                                                        <i class="fa fa-list" aria-hidden="true">

                                                        </i>
                                                        المنتجات
                                                    </h3>
                          </div>
          <form  action="{{url('/admin-panel/order/update_order_product')}}" method="post">
            {{ csrf_field() }}
              <input type="hidden" name="order_id" value="{{ $order->id }}">
                          <div class="col-lg-3">
                              <label for="">اضافة منتج الى الاوردر</label>

                              <!-- <div style="width:520px;margin:0px auto;margin-top:30px;"> -->
                             <select class="chosen" name="product_id" style="width:100%;">

                               @foreach($order->products as $product)
                                  <option value="{{$product->id}}" >{{$product->title}}</option>
                               @endforeach

                             </select>
                          </div>

                          <div class="col-lg-3">
                            <label for="">الكمية</label>
                            <input type="text" name="quantity" placeholder="ادخل الكمية" required>
                          </div>

                          <div class="col-lg-2">
                            <button class="btn btn-success" type="submit" >اضافة</button>
                          </div>

                      </div>
</form>

                    </div>
                    <div class="box-body box-profile">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                              <th>الاسم</th>
                              <th>الحجم</th>
                                <th>السعر</th>
                                <th>الكميه</th>
                                <th>الاجمالى</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->orderProducts as $product)
                                <tr>
                                  <td>{{$product->product->title }}</td>
                                  <td>{{$product->size }}</td>
                                    <td>{{$product->total_price/$product->qua }}</td>
                                    <td>{{$product->qua }}</td>
                                    <td> @php echo round($product->total_price,2) @endphp
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>


                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            <i class="fa fa-list" aria-hidden="true">

                            </i>
                            حاله الطلب
                        </h3>
                    </div>


                    <form class="form-horizontal">
                        <div class="box-body box-profile ">


                            <div class="form-group">
                                <label class="control-label col-sm-2"> الحاله:</label>
                                <div class="col-sm-4">
                                    <select name="status_id" class="form-control" id="selected_status">
                                        @foreach($statuses as $status)
                                            <option value="{{$status->id}}">{{$status->title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-2"> الملاحظات:</label>
                                <div class="col-sm-6">
                                <textarea class="form-control" id="com" rows="4">
                                </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-primary pull-right"
                                            id="update-status">
                                        {{trans('configmodule::settings.update')}} &nbsp;
                                        <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>


                            <div class="added_status" style="margin-top: 50px;">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>الحاله</th>
                                        <th>التاريخ</th>
                                        <th>التوقيت</th>
                                        <th>التعليقات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($order->status as $status)
                                          <tr>
                                              <td>{{$status->title}}</td>
                                              <td>{{$status->pivot->created_at }} </td>
                                              <td>{{$status->pivot->created_at->diffForHumans()}}</td>
                                              <td>{{$status->pivot->status_comment}}</td>
                                          </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-truck" aria-hidden="true"></i> الشحن</h3>
                    </div>
                    <!-- /.box-header -->

                    <form class="form-horizontal">
                        <div class="box-body">

                            <div class="col-md-8">
                                <div class="form-group">
                                    {{--<div class="callout callout-success">--}}
                                    {{--<label class="com">لقد تم اعداد حالة شحن المنتج مسبقاً &nbsp;&nbsp;&nbsp;--}}
                                    {{--[<i class="fa fa-check" aria-hidden="true"></i>]</label>--}}
                                    {{--</div>--}}
                                </div>

                                <div class="form-group ">
                                    <label class="control-label col-sm-2 show_ship" for="driver"> السائق:</label>
                                    <div class="col-sm-8 show_ship">
                                        <select name="status_id" class="form-control" id="selected_driver">
                                            @foreach($drivers as $driver)
                                                <option value="{{$driver->id}}">{{$driver->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="added_status" style="margin-top: 50px;">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>السائق</th>
                                            <th>رقم السائق</th>
                                            <th>التاريخ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order->ship_driver as $ship)
                                            <tr>
                                                <td>{{$ship->driver->name}}</td>
                                                <td>{{$ship->driver->phone}}</td>
                                                <td>{{$ship->created_at }} </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>


                            </div>
                            <br>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer ">
                            <button type="submit" id="shipping" class="btn btn-warning pull-right show_ship">شحن &nbsp;
                                <i class="fa fa-truck"></i>
                            </button>
                        </div>
                        <!-- /.box-footer -->
                    </form>

                </div>
            </div>
        </div>

    </section>

@endsection

@section('javascript')

    @include('commonmodule::includes.swal')
    @include('ordermodule::includes.update_status_js')
    @include('ordermodule::includes.createOrderShipping')

    <script>
        $(document).ready(function() {
            $(document).on('click', '#update_date-time', function(e) {
                var $tr = $(this).closest('tr')
                id = $tr.find('#order_id').val()

                $.ajax({
                    'type': 'post',
                    'url': '{{ url("admin-panel/order/update-payment") }}',
                    data: {
                        '_token': '{{csrf_token()}}',
                        'id': id,
                    },
                    'statusCode': {
                        200: function (response) {
                            swal("رائع", "تم تحديث   بنحاح!", "success", { button: "Ok", });
                            $('#myTable').DataTable().ajax.reload();

                        },
                        422: function (response) {
                            swal("خطأ", "حدث خطأ ما، أعد ادخال البيانات الصحيحة", "error", { button: "Ok", });
                        }
                    },
                })
            })
        })
    </script>

    <script type="text/javascript">
    $(".chosen").chosen();
    </script>

@endsection
