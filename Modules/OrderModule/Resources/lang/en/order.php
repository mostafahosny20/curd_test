<?php

return array(

    'pagetitle'      => 'Orders',
    'homepage'       => 'Home Page',
    'addnew'         => 'Add New',
    'id'             => 'ID',
    'operation'      => 'Operations',
    'delivery'       => 'Delivery',
    'customer'       => 'Customer',
    'total'          => 'Total',
    'status'         =>  'Status',
    'date'           =>  'Date',
    'cancel'         => 'Cancel',
    'submit'         => 'Submit',
    'view'           => 'view',
    'print'           => 'print',
    'delete'         => 'Delete',
    'product'        => 'Product',
    'quantity'       => 'Quantity',
    'order_id'       => 'Order',
);
