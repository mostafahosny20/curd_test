<?php
return [
    'title' => "Order Types",
    'id' => "ID",
    'status' => "Status",
    'type' => "Type",
    'type_select' => "Select Type",

    'operation' => "Operations",
    'submit' => "Submit",
    'cancel' => "Cancel",
];