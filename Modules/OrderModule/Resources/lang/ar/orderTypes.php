<?php
return [
    'title' => "انواع الاوردرات",
    'id' => "رقم الحاله",
    'status' => "الحاله",
    'type' => "النوع",
    'type_select' => "اختر النوع",
    'operation' => "العمليات",
    'submit' => "حفظ",
    'cancel' => "الغاء",
];