<?php

return array(

    'pagetitle' => 'الاوردرات',
     'homepage' => 'الرئيسية',
    'addnew' => 'أضف جديد',
    'id' => 'رقم الاوردر',
    'operation' => 'العمليات',
    'delivery' => 'التوصيل',
    'discount' => 'الخصم',
    'customer' => 'العميل',
    'total' => 'الاجمالى',
    'status' =>  'الحاله',
    'date' =>  'التاريخ',
    'view'=> 'عرض',
    'delete'=> 'حذف',
    'print'           => 'طباعه',
    'tax'           => 'الضرائب',

    'cancel' => 'الغاء',
    'submit' => 'حفظ',

    'product_id'        => 'رقم مالمنتج',
    'product'        => 'المنتج',
    'quantity'       => 'الكمية',
    'order_id'       => 'الاوردر',
    'city'=>"المدينه",
    'zone'=>"الحى"
);
