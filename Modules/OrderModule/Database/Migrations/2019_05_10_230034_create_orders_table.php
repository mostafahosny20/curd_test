<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id')->nullable()->unsigned();
          $table->string('payment_status')->nullable()->default('unpaid');
          $table->double('sub_total')->nullable();
          $table->double('discount')->nullable();
          $table->double('wallet')->nullable();
          $table->double('shipping')->nullable();
          $table->double('tax')->nullable();
          $table->double('total')->nullable();

          $table->integer('current_status_id')->unsigned()->default(\App\Enums\OrderStatuses::NEW);
          $table->integer('current_status_type_id')->unsigned()->nullable()->default(\App\Enums\OrderStatusType::OPEN);
          $table->foreign('current_status_type_id')->references('id')->on('status_types')->onDelete('set null');

          $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
