<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('order_id')->unsigned()->nullable();
          $table->string('save_use_package')->nullable();
          $table->integer('user_address_id')->unsigned();
          $table->string('delivery_date')->nullable();
          $table->string('delivery_time')->nullable();
          $table->string('coupon_code')->nullable();
          $table->double('delivery_ship_price')->nullable();
          $table->integer('month_subscribe')->default(1);
          $table->string('payment_type')->nullable();
          $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
