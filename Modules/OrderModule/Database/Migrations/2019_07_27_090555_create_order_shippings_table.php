<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_shippings', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('order_id')->unsigned()->nullable();
          $table->integer('driver_id')->unsigned()->nullable();
          $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
          $table->foreign('driver_id')->references('id')->on('shipping_driver')->onDelete('set null');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_shippings');
    }
}
