<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_products', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('order_id')->unsigned()->nullable();
          $table->integer('product_id')->unsigned()->nullable();
          $table->string('size')->unsigned()->nullable();
          $table->string('quantity')->default(0);
          $table->double('total_price')->nullable();
          $table->foreign('order_id')->references('id')->on('orders')->onDelete('set null');
          $table->foreign('product_id')->references('id')->on('products')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_products');
    }
}
