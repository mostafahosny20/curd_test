<?php

namespace Modules\OrderModule\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderShipping extends Model
{


  protected $fillable = ['order_id', 'driver_id'];


  function order()
  {
      return $this->belongsTo(Order::class, 'order_id', 'id');
  }


  function driver(){
      return $this->belongsTo(ShippingDriver::class,'driver_id','id');
  }


}
