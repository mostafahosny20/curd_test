<?php

namespace Modules\OrderModule\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
  protected $table='order_statuses';
  protected $fillable = ["order_id","status_id",'status_type_id',"status_comment"];
  // protected $table = "order_statuses";

    public function order(){
      return $this->belongsTo(Order::class,'order_id');
    }


    public function status(){
      return $this->belongsTo(Status::class,'status_id');
    }

}
