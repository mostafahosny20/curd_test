<?php

namespace Modules\OrderModule\Entities;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['status_id'];


    function orders(){
        return $this->belongsToMany(Order::class,"order_status","status_id","order_id");
    }

    function status_type(){
        return $this->belongsTo(StatusTypes::class,'status_type_id');
    }

}
