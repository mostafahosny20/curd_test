<?php

namespace Modules\OrderModule\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
  protected $fillable = [
      "order_id",
      "payment_type",
      "delivery_ship_price",
      "user_address_id",
      "delivery_date",
      "delivery_time",
      "coupon_code",
      "month_subscribe",
  ];



    function order(){
        return $this->belongsTo(Order::class);
    }

    function userAddresses(){
        return $this->belongsTo(UserAddress::class,'user_address_id');
    }

}
