<?php

namespace Modules\OrderModule\Entities;
use Modules\UserModule\Entities\User;
use Modules\ProductModule\Entities\Product;
use Modules\UserModule\Entities\UserAddress;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $fillable = [
      "user_id",
      "sub_total",
      "discount",
      'wallet',
      'shipping',
      "total",
      'tax',
      'payment_status',
      "current_status_id",
      'current_status_type_id'
  ];


    public function user()
    {
      return $this->belongsTo(User::class,'user_id');
    }

    public function orderDetail()
    {
      return $this->hasMany(OrderDetail::class);
    }

    // public function orderStatus()
    // {
    //   return $this->hasMany(OrderStatus::class,'order_id');
    // }

    function currentStatus()
    {
        return $this->belongsTo(Status::class, "current_status_id", "id");
    }

    function Status()
    {
        return $this->belongsToMany(Status::class, "order_statuses", "order_id", "status_id")
            ->withTimestamps()->withPivot("status_comment")->orderBy('id', 'desc');
    }

    function orderStatus()
    {
        return $this->hasMany(OrderStatus::class, 'order_id');
    }


    public function orderProducts()
    {
      return $this->hasMany(OrderProduct::class,'order_id');
    }


    public function products()
    {
      return $this->belongsToMany(Product::class,"order_products", "order_id", "product_id")->withPivot('qua','size','total_price');
    }


    function userAddresses()
    {
        return $this->belongsToMany(UserAddress::class, "order_details", "order_id", "user_address_id");
    }

    function ship_driver()
    {

        return $this->hasMany(OrderShipping::class, 'order_id', 'id');
    }






}
