<?php

namespace Modules\OrderModule\Entities;

use Illuminate\Database\Eloquent\Model;

class ShippingDriver extends Model
{

  protected $table = 'shipping_driver';
  protected $fillable = [];
}
