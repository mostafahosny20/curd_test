<?php
/**
 * Created by PhpStorm.
 * User: hossam
 * Date: 1/25/19
 * Time: 5:09 PM
 */

namespace Modules\OrderModule\Repository;

use App\Enums\OrderStatuses;
// use Modules\CommonModule\Helper\BaseHelper;
use Modules\OrderModule\Entities\Order;
use Modules\OrderModule\Entities\OrderShipping;

class OrderRepository
{

    // use BaseHelper;



    function find($id)
    {
      return Order::find($id);
    }

    function findOrders($status){

      if($status=='current')
      return  $this->current();
      else if($status=='done')
      return    $this->done();
      else
    return  $this->cancel();

    }

    function current()
    {
      return Order::where('current_status_type_id',1)->with(['currentStatus','user'])->get();
    }

    // function current(){
    //
    //   Order::where('current_status_id',OrderStatuses::_NEW)
    //         ->orWhere('current_status_id',OrderStatuses::UNDER_PREPARATION)
    //         ->orWhere('current_status_id',OrderStatuses::READY)
    //         ->orWhere('current_status_id',OrderStatuses::UNDER_SHIPPING)
    //         ->get();
    // }

    function done(){

      return Order::where('current_status_type_id',2)->with(['currentStatus','user'])->get();

    }

    function cancel(){
      return Order::where('current_status_type_id',3)
                    ->orWhere('current_status_type_id',4)
      ->with(['currentStatus','user'])->get();

    }






    function saveOrder($data)
    {

      return Order::create($data);

    }
    function userOrders($user_id)
    {
      return Order::with(['user', "orderDetail", 'products','userAddresses','currentStatus'])
          ->where('user_id', $user_id)->orderBy('id', 'desc')->get();

    }

    function orderDetails($order_id,$user_id)
    {
      return Order::with(['user', "orderDetail", 'products','userAddresses','currentStatus'])
          ->where('user_id', $user_id)->where('id', $order_id)->orderBy('id', 'desc')->first();

    }

    function orderDetails1($order_id)
    {
      return Order::with(['user', "orderDetail", 'products','userAddresses','orderStatus'])
          ->where('id', $order_id)->orderBy('id', 'desc')->first();

    }

    function update($order, $data)
    {
        $order = $order->update($data);
        return $order;
    }








    // return all orders count
    function allOrdersCount()
    {

    }

    //function to retuen all new orders count

    function allNewOrdersCount()
    {

    }

    // function to return all preparing orders count

    function allPreparingOrdersCount()
    {

    }

    // function to return all prepared orders count

    function allPreparedOrdersCount()
    {

    }

    // function to return all shipping orders count

    function allShippingOrdersCount()
    {

    }

    // function to return all shipped orders count

    function allShippedOrdersCount()
    {

    }

    // function to return all done orders count

    function allDoneOrdersCount()
    {

    }

    // function to return all cancelled orders count

    function allCancelledOrdersCount()
    {

    }

    // function to return current order





}
