<?php
/**
 * Created by PhpStorm.
 * User: hossam
 * Date: 1/25/19
 * Time: 5:09 PM
 */

namespace Modules\OrderModule\Repository;


use Modules\CommonModule\Helper\BaseHelper;
use Modules\OrderModule\Entities\Order;
use Modules\OrderModule\Entities\OrderStatus;
use Modules\OrderModule\Entities\Status;

class OrderStatusRepository
{
    public function all()
    {
        return Status::all();
    }

    public function saveStatusWithComment($order, $orderStatus, $comment)
    {
        $status_type_id=$this->findStatusType($orderStatus);

        return $order->orderStatus()->create(['status_id' => $orderStatus,'status_type_id' => $status_type_id->status_type_id ,'status_comment' => $comment]);
    }

    public function save($data)
    {

        return Status::create($data);
    }

    public function saveStatus($order, $orderStatus)
    {
        return $order->orderStatus()->create(['status_id' => $orderStatus]);
    }

    public function deleteAllStatusesOfOrder($order_id)
    {
        OrderStatus::destroy($order_id);
    }

    public function findById($status_id)
    {
        return Status::where('id', $status_id)->first(['title']);
    }

    function findStatusType($status_id)
    {
        return Status::where('id', $status_id)->first(['status_type_id']);

    }


}
