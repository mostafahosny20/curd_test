<?php
/**
 * Created by PhpStorm.
 * User: hossam
 * Date: 1/25/19
 * Time: 5:09 PM
 */

namespace Modules\OrderModule\Repository;


// use Modules\CommonModule\Helper\BaseHelper;
use Modules\OrderModule\Entities\Order;
use Modules\OrderModule\Entities\OrderProduct;

class OrderProductRepository
{

   // use BaseHelper;

   public function save($data){
       OrderProduct::create($data);
   }
    public function saveProducts($order,$orderProducts){

      $products=[];
          foreach ($orderProducts as $key => $value) {
            foreach ($value['product_prices'] as $key => $price) {
              if($price['qua']==0)continue;
              // $price['order_id']=$order['id'];
              $price['total_price']=$price['price']*$price['qua'];
                array_push($products,$price);
            }
          }

         return $order->orderProducts()->createMany($products);
    }

    function deleteOrderProducts($order_id){
       OrderProduct::destroy($order_id);
    }


    public function findAllOrderProduct($order_id){


       $products=OrderProduct::where('order_id',$order_id)->get(['product_id','quantity']);

       return $products->toArray();
    }



}
