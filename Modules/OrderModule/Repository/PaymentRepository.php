<?php


namespace Modules\OrderModule\Repository;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use URL;
use Illuminate\Support\Facades\Redirect;
use Session;
use Modules\OrderModule\CustomFacades\OrderOperations;
use Illuminate\Http\Request;
class PaymentRepository
{

  private $_api_context;

  public function __construct()
  {
      /** setup PayPal api context **/
      $paypal_conf = \Config::get('paypal');
      $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
      $this->_api_context->setConfig($paypal_conf['settings']);

  }

  public function paypalRedirect(Request $request)
  {

     $redirect_url=$this->postPaymentWithpaypal();

      return Redirect::away($redirect_url);


    // die('iam here');

    // return $this->setCode(200)->setData($paypal)->send();
  }

//Store a details of payment with paypal.

  public function postPaymentWithpaypal($request,$user_id)

  {
    $total=(int) $request->get('orderMoney')['total'];
    $sub_total=(int) $request->get('orderMoney')['sub_total'];
    $shipping=(int) $request->get('orderMoney')['shipping'];


      $payer = new Payer();
      $payer->setPaymentMethod('paypal');

      $item_1 = new Item();
      $item_1->setName('Item 1') /** item name **/
          ->setCurrency('USD')
          ->setQuantity(1)
          ->setPrice($total); /** unit price **/

      $item_list = new ItemList();
      $item_list->setItems(array($item_1));


      $details = new Details();
      $details->setShipping(0)
              ->setTax(0)
              ->setSubtotal($total);

      $amount = new Amount();
      $amount->setCurrency("USD")
             ->setTotal($total)
             ->setDetails($details);

      $transaction = new Transaction(); // بيديك رقم العملية علشان لو بعد كدا حصل حاجه بتروح باي بال تقوله انا معاايا رقم العملية وهكذا
      $transaction->setAmount($amount)
                  ->setItemList($item_list)
                  ->setDescription($user_id);


//            كل وظيفتة ان لما عملية الدفع تتم  بيرجع على الموقع بتاعى تانى ومنها اعرف العملية ناجحة ولا لا
                  // $baseUrl = url('/');
                  // $redirect_urls = new RedirectUrls();
                  // $redirect_urls->setReturnUrl("$baseUrl/ExecutePayment.php?success=true") // لو تمت هيرجع من هنا
                  //     ->setCancelUrl("$baseUrl/ExecutePayment.php?success=false"); // لو كانسل يرجع هنا

                  $redirect_urls = new RedirectUrls();
                  $redirect_urls->setReturnUrl(URL::to('status?success=true')) /** Specify return URL **/
                  ->setCancelUrl(URL::to('/'));
                  // ->setCancelUrl(URL::to('status?success=false'));

          $payment = new Payment();
          $payment->setIntent('Sale')
              ->setPayer($payer)
              ->setRedirectUrls($redirect_urls)
              ->setTransactions(array($transaction));

          /** dd($payment->create($this->_api_context));exit; **/


      try {

           $payment->create($this->_api_context);

      } catch (\PayPal\Exception\PPConnectionException $ex) {

    echo '<pre>';print_r(json_decode($pce->getData()));exit;

          if (\Config::get('app.debug')) {
              \Session::put('error','Connection timeout');
              return Redirect::route('addmoney.paywithpaypal'); /////////////////////////////////////////

              /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/

              /** $err_data = json_decode($ex->getData(), true); **/

              /** exit; **/

          } else {

              \Session::put('error','Some error occur, sorry for inconvenient');

              return Redirect::route('addmoney.paywithpaypal');

              /** die('Some error occur, sorry for inconvenient'); **/

          }

      }

      foreach($payment->getLinks() as $link) {

          if($link->getRel() == 'approval_url') {

              $redirect_url = $link->getHref();

              break;

          }

      }


      /** add payment ID to session **/

      Session::put('paypal_payment_id', $payment->getId());


      // $r=Session::get('paypal_payment_id');
      // echo $r;
      // die('error');


      if(isset($redirect_url)) {

          /** redirect to paypal **/

return $redirect_url;
          // return Redirect::away($redirect_url);
      }

      \Session::put('error','Unknown error occurred');

      return Redirect::route('addmoney.paywithpaypal');

  }

  public function getPaymentStatus1()

  {


      /** Get the payment ID before session clear **/

      $payment_id=Session::get('paypal_payment_id');
      Session::forget('paypal_payment_id'); //clear the session payment ID


      if (empty($_GET['PayerID']) || empty($_GET['token'])) {
          \Session::put('error','Payment failed');
          return Redirect::route('addmoney.paywithpaypal');

      }

      $payment = Payment::get($payment_id, $this->_api_context);

      /** PaymentExecution object includes information necessary **/

      /** to execute a PayPal account payment. **/

      /** The payer_id is added to the request query parameters **/

      /** when the user is redirected from paypal back to your site **/

      $execution = new PaymentExecution();

      $execution->setPayerId($_GET['PayerID']);

      /**Execute the payment **/

      $result = $payment->execute($execution, $this->_api_context);

      /** dd($result);exit; /** DEBUG RESULT, remove it later **/

      if ($result->getState() == 'approved') {



          /** it's all right **/

          /** Here Write your database logic like that insert record or value in database if you want **/

          \Session::put('success','Payment success');

          return Redirect::route('addmoney.paywithpaypal');

      }

      \Session::put('error','Payment failed');

      return Redirect::route('addmoney.paywithpaypal');

  }


  public function getPaymentStatus(){

try {


    if (isset($_GET['success']) && $_GET['success'] == 'true') {


          $paymentId = $_GET['paymentId'];
          $payment = Payment::get($paymentId, $this->_api_context);

          $execution = new PaymentExecution();
          $execution->setPayerId($_GET['PayerID']);



          $transaction = new Transaction();
          $amount = new Amount();
          $details = new Details();
          //

          $user_id=(int) $payment->transactions[0]->description;
          $my_file = 'tempFiles/'.$user_id.'.txt';
          $handle = fopen($my_file, 'r');
          $data = fread($handle,filesize($my_file));
          $d=$this->stdToArray(((array) json_decode($data)));

          $request = new Request($d);
          $total=$request->orderMoney['total'];



          $details->setShipping(0)
              ->setTax(0)
              ->setSubtotal($total);

          $amount->setCurrency('USD');
          $amount->setTotal($total);
          $amount->setDetails($details);
          $transaction->setAmount($amount);

           $execution->addTransaction($transaction);



    try {

              $result = $payment->execute($execution, $this->_api_context);

               // ResultPrinter::printResult("Executed Payment", "Payment", $payment->getId(), $execution, $result);

                try { $payment = Payment::get($paymentId, $this->_api_context);

                } catch (Exception $ex) {
                  echo 'ggggggggggggg';
                  // ResultPrinter::printError("Get Payment", "Payment", null, null, $ex);
                exit(1);
                }
                } catch (Exception $ex) {

                  // print_r($ex);
                  echo "error from mostafa";
                  // ResultPrinter::printError("Executed Payment", "Payment", null, null, $ex);
                exit(1);
                }

                echo " تمت عملية الدفع بنجاح جارى التوجيه";

                // ResultPrinter::printResult("Get Payment", "Payment", $payment->getId(), null, $payment);

                $request['payment']=$payment;


                return $request;

                } else {
                    echo "User Cancelled the Approval";
                exit;
                }

              } catch (\Exception $e) {
                  return -1;
              }
  }


  function stdToArray($obj){
    $reaged = (array)$obj;
    foreach($reaged as $key => &$field){
      if(is_object($field))$field = $this->stdToArray($field);
    }
    return $reaged;
  }


}
