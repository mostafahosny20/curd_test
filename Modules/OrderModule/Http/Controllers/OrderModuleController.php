<?php

namespace Modules\OrderModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\OrderModule\Repository\OrderRepository;
use Modules\OrderModule\Repository\OrderStatusRepository;
use Yajra\DataTables\DataTables;
use Modules\OrderModule\Entities\Order;
use Modules\OrderModule\Entities\Status;
use Modules\OrderModule\Entities\ShippingDriver;
use Modules\CommonModule\Helper\ApiResponseHelper;
use App\Events\OrderStatusChange;

class OrderModuleController extends Controller
{

      use ApiResponseHelper;



  private $orderRepo;
  private $orderStatusRepo;
  public function __construct(OrderRepository $orderRepository,OrderStatusRepository $orderStatus){
    $this->orderRepo=$orderRepository;
    $this->orderStatusRepo=$orderStatus;
  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($status)
    {
      // $o= Order::where('current_status_type_id',1)->get();
      // echo "<pre>";
      // print_r($o);
      // echo "</pre>";
      //
      // die();
        return view('ordermodule::index',compact('status'));
    }
    public function getOrders($status){

      $orders=$this->orderRepo->findOrders($status);


    return Datatables::of($orders)
        ->addColumn('id',function($order){
          return $order->id;
        })
        ->addColumn('customer',function($order){
          return $order->user->name;
        })
        ->addColumn('total',function($order){
          return $order->total;
        })
        ->addColumn('status',function($order){
          return $order->currentStatus->title;
        })
        ->addColumn('date',function($order){
          return $order->created_at;
        })
        ->addColumn('view', function($order) {
            return '<a href="'. url("admin-panel/order/" . $order->id) .'" type="button" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>';
        })
        ->addColumn('print', function($order) {
            return '<a href="'. url("admin-panel/order/" . $order->id."/print") .'" type="button" class="btn btn-info"><i class="fa fa-print" aria-hidden="true"></i></a>';
        })
        ->rawColumns(['view' => 'view' ,'print' => 'print'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */

    public function updateStatus(Request $request){
      $order_id=$request->order_id;
      $status_id=$request->status_id;
      $comment=$request->comment;

      $order=Order::with(['currentStatus'])->where('id',$order_id)->first();

      $status=$this->orderStatusRepo->saveStatusWithComment($order,$status_id,$comment);
      $status_type_id=$this->orderStatusRepo->findStatusType($status_id);

      // update status in order status
      $updateStatus=$this->orderRepo->update($order,['current_status_type_id'=>$status_type_id->status_type_id,'current_status_id'=>$status_id]);

      // die($updateStatus);

      event(new OrderStatusChange($order));



          return $this->setCode(200)->setData($status)->send();

    }


    public function create()
    {
        return view('ordermodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
      $order=$this->orderRepo->orderDetails1($id);
      $statuses=Status::all();
      $drivers=ShippingDriver::all();
        return view('ordermodule::show',compact('order','statuses','drivers'));
    }

    public function getOrderStatus()
    {
        $statuses=Status::all();
        return view('ordermodule::order_types.index',compact('statuses'));

    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('ordermodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
