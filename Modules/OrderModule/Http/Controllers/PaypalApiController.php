<?php

namespace Modules\OrderModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\CommonModule\Helper\ApiResponseHelper;
use Modules\OrderModule\Repository\PaymentRepository;
use JWTAuth;
use Illuminate\Support\Facades\Redirect;
use Modules\OrderModule\CustomFacades\OrderOperations;


class PaypalApiController extends Controller
{

  private $user;
  private $user_id;
  private $paypal_repo;
  use ApiResponseHelper;

  public function __construct(PaymentRepository $payment)
  {
    // $this->user=JWTAuth::parseToken()->authenticate();
    // $this->user_id=$this->user->id;
    $this->paypal_repo=$payment;
  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function paypalRedirect(Request $request)
    {

       $redirect_url=$this->paypal_repo->postPaymentWithpaypal();

        return Redirect::away($redirect_url);


      // die('iam here');

      // return $this->setCode(200)->setData($paypal)->send();
    }

    public function getPaymentStatus(){
      $request=$this->paypal_repo->getPaymentStatus();

      if(gettype($request) =="object" && $request->payment->state=="approved")
      {
        $order=$this->saveOrder($request);
        echo "<a href='/#/order_details/$order->id'>اذا لم يتم التوجية اضغط هنا</a>";
        return redirect('/#/orderdone/'.$order->id);
      }
      else
      {
        $request=$this->paypal_repo->getPaymentStatus();
      }
    }




      public function saveOrder($request){
        $user_id=(int) $request->payment->transactions[0]->description;
        // dd($request->payment->transactions[0]->description);
        return OrderOperations::save($request,$user_id);
        // save the transaction process
        // return $this->setCode(200)->setData($request->all())->setSuccess($order)->send();
      }



}
