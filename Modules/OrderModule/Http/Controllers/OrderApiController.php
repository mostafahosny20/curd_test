<?php

namespace Modules\OrderModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\RedirectResponse;

use Illuminate\Routing\Controller;
use Modules\OrderModule\CustomFacades\OrderOperations;
use Modules\CommonModule\Helper\ApiResponseHelper;
use Modules\OrderModule\Repository\OrderRepository;
use Modules\OrderModule\Repository\OrderStatusRepository;
use Modules\OrderModule\Repository\PaymentRepository;
use JWTAuth;
use Modules\OrderModule\Entities\Order;
use App\Events\OrderStatusChange;
use Illuminate\Support\Facades\Redirect;
use Session;
use Url;
class OrderApiController extends Controller
{

  private $user;
  private $user_id;
  private $orderRepository;
  private $orderStatusRepo;
  private $paypal_repo;

  use ApiResponseHelper;


  public function __construct(OrderRepository $orderRepo,OrderStatusRepository $orderStatusRepo,PaymentRepository $payment)
  {
    $this->user=JWTAuth::parseToken()->authenticate();
      $this->user_id=$this->user->id;
      $this->orderRepository=$orderRepo;
      $this->orderStatusRepo=$orderStatusRepo;
      $this->paypal_repo=$payment;

  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $user_orders=$this->orderRepository->userOrders($this->user_id);
        return $this->setCode(200)->setData($user_orders)->send();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('ordermodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */


    public function store(Request $request)
    {




        if($request->get("orderDetails")['payment_type']=="visa")
        {

          $my_file = 'tempFiles/'.$this->user_id.'.txt';
          $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
          $data = json_encode($request->all());
          fwrite($handle, $data);


          $redirect_url= $this->paypal_repo->postPaymentWithpaypal($request,$this->user->id);

          return $this->setCode(201)->setData($redirect_url)->send();

        }

        $order=OrderOperations::save($request,$this->user->id);
        return $this->setCode(200)->setData($request->all())->setSuccess($order)->send();

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

      $order=$this->orderRepository->orderDetails($id,$this->user_id);

      if($order)
        return $this->setCode(200)->setData($order)->send();
      else
      return $this->setCode(400)->SetError("هذا الاوردر غير موجود")->send();
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('ordermodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function storeStatus ()
    {

    }


    public function findOrder($id)
    {

        $order=$this->orderRepository->find($id);
        if($order)
          return $this->setCode(200)->setData($order)->send();
        else
        return $this->setCode(400)->SetError("هذا الاوردر غير موجود")->send();

    }

    public function cancelOrder($id)
    {
      $order_id=$id;
      $status_id=7;
      $comment='الغاء';

      $order=Order::with(['currentStatus'])->where('id',$order_id)->first();

      $status=$this->orderStatusRepo->saveStatusWithComment($order,$status_id,$comment);
      $status_type_id=$this->orderStatusRepo->findStatusType($status_id);

      // update status in order status
      $updateStatus=$this->orderRepository->update($order,['current_status_type_id'=>$status_type_id->status_type_id,'current_status_id'=>$status_id]);

      // die($updateStatus);

      event(new OrderStatusChange($order));



      return $this->setCode(200)->setData($status)->send();



    }

}
