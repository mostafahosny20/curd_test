<?php

namespace Modules\OrderModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\OrderModule\Repository\ShippingDriverRepository;

class ShippingDriverController extends Controller
{

  private $driverRepo;
  public function __construct(ShippingDriverRepository $driverRepo){
    $this->driverRepo=$driverRepo;
  }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $drivers=$this->driverRepo->findAllDrivers();

        return view('ordermodule::shipping_driver.index',compact('drivers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('ordermodule::shipping_driver.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('ordermodule::shipping_driver.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('ordermodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function AssignDriver(Request $request) {
      $data=$request->except('_token');

      $driver=$this->driverRepo->AddOrderDirver($data);

      return $this->setCode(200)->setData($driver)->send();

    }

}
