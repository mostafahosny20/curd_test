<?php

namespace Modules\OrderModule\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;


class OrderServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        App::bind('OrderOperations', function()

        {

            return new \Modules\OrderModule\Services\OrderOperations;

        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
