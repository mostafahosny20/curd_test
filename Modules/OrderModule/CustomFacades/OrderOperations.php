<?php
/**
 * Created by PhpStorm.
 * User: hossam
 * Date: 1/26/19
 * Time: 12:21 AM
 */

namespace Modules\OrderModule\CustomFacades;


use Illuminate\Support\Facades\Facade;

class OrderOperations extends  Facade {


    protected static function getFacadeAccessor() { return 'OrderOperations'; }
}