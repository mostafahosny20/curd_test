<?php
/**
 * Created by PhpStorm.
 * User: hossam
 * Date: 1/26/19
 * Time: 12:34 AM
 */

namespace Modules\OrderModule\Services;


use App\Enums\OrderStatuses;
use Illuminate\Http\Request;
use Modules\OrderModule\Repository\OrderDetailRepository;
use Modules\OrderModule\Repository\OrderProductRepository;
use Modules\OrderModule\Repository\OrderRepository;
use Modules\OrderModule\Repository\OrderStatusRepository;
// use App\Events\OrderStatusChanged;

class OrderOperations
{

  /**
   * @var OrderRepository
   */
  private $orderRepository;
  /**
   * @var OrderDetailRepository
   */
  private $orderDetailRepository;
  /**
   * @var OrderProductRepository
   */
  private $orderStatusRepository;
  /**
   * @var OrderProductRepository
   */
  private $orderProductRepository;




  public function save(Request $request,$user_id)
  {

    $this->orderRepository = new OrderRepository;
    $this->orderProductRepository = new OrderProductRepository();
    $this->orderDetailRepository = new OrderDetailRepository();
    $this->orderStatusRepository = new OrderStatusRepository();

    $orderProducts = $request->get("orderProducts");

    $order = $this->addOrder($request,$user_id);
            // event(new OrderStatusChanged($order));
    if ($order != null){
         $this->addOrderDetails($order,$request);
         $this->addOrderProducts($order,$request);
         // $this->addOrderStatus($order,$request);
    }
    // $order = $order->with(['user',"packages","orderDetail","userAddresses",'products','status'])->where('id',$order->id)->first();

    return $order;


  }

  function addOrder(Request $request,$user_id){
      $order=$request->get('orderMoney');
      $order['user_id']=$user_id;
      // echo "<pre>";
      // print_r($order);
      // echo "</pre>";

      try{
          return $this->orderRepository->saveOrder($order);
      }catch (\Exception $exceptione){
          return null;
      }

  }

  private function addOrderDetails($order,$request){
      $orderDetails = $request->get("orderDetails");
     return $this->orderDetailRepository->saveMany($order,$orderDetails);
  }
  private function addOrderProducts($order,$request){
    // $orderProducts = $request->get("orderProducts");
    $orderProducts = json_decode(json_encode($request->orderProducts),true);


      return $this->orderProductRepository->saveProducts($order,$orderProducts);
  }

  public function addOrderStatus($order,$request){
      $orderStatus = 1;
      return $this->orderStatusRepository->saveStatus($order,$orderStatus);
  }



}
