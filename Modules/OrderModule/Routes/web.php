<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin-panel', 'middleware' => ['auth:admin']], function() {

  Route::resource('/order', 'OrderModuleController');


      Route::get('/order-ajax/{status}', 'OrderModuleController@getOrders');


      Route::get('/orders/{status}', 'OrderModuleController@index');


      Route::post('order/update-status', 'OrderModuleController@updateStatus');

      Route::resource('/shipping/driver', 'ShippingDriverController');

      Route::post('order/shipping', 'ShippingDriverController@AssignDriver');



});

Route::group(['middleware' => ['web']], function() {

  Route::get('admin-panel/order-status-types', 'OrderModuleController@getOrderStatus');

  Route::get('order/saca', 'PaypalApiController@saveOrder');

Route::get('status', 'PaypalApiController@getPaymentStatus');

Route::get('subscribe/paypal', 'PaypalApiController@paypalRedirect')->name('paypal.redirect');
Route::get('subscribe/paypal/return', 'PaypalApiController@paypalReturn')->name('paypal.return');
});
