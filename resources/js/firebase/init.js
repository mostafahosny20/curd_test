import firebase from 'firebase/app';
import firestore from 'firebase/firestore';


var config = {
    apiKey: "AIzaSyBZzXWJGghusqvBnE3P9cci8cWykJCY0Z0",
    authDomain: "chat-7f8e4.firebaseapp.com",
    databaseURL: "https://chat-7f8e4.firebaseio.com",
    projectId: "chat-7f8e4",
    storageBucket: "chat-7f8e4.appspot.com",
    messagingSenderId: "881289755928"
};
const firebaseApp=firebase.initializeApp(config);
firebaseApp.firestore().settings({timestampsInSnapshots:true});

export default firebaseApp.firestore();
