import Vue from 'vue'
import Vuex from 'vuex'
import toasted from "vue-toasted";

import _ from "lodash";

import {
    encryptUser,restCart
} from './components/shared/service/authService'

import { infoToaster, successToaster ,errorToaster} from "./components/shared/service/ErrorHandler";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
      cartProducts: [],
        loggedUser: {},
        msg:'',
        // totalValue: 0.0,
        // totalProductPrice:0.0,
        deliveryCost:10,
        useraddress:{},
        orderDetails:{},
        orderProducts:{},
        orderMoney: {
            sub_total:0.0,
            discount:0.0,
            wallet:1,
            shipping:30,
            tax:1,
            total:0
        },

    },

    getters: {
        cartProducts: state => {
            return state.cartProducts
        },
        getLoggedUser: state => {
            return state.loggedUser
        },
        getuseraddress:state=>{
          return state.useraddress


        }
    },

    mutations: {
        ADD_CART_LOCAL: (state, product) => {

            state.cartProducts.push(product);
            localStorage.setItem('iki-cart', JSON.stringify(state.cartProducts))
            state.msg='تم الاضافة بنجاح';
            successToaster(state.msg, "");

        },


        CHANGE_QUANTITY:(state,{item,price,quan}) => {
        const proPrice = _.find(item.product_prices,{ price: price});
        if(quan==0) {
          proPrice.qua=0;

        }
        else if(proPrice.qua==1&&quan==-1){return;}

        else
        proPrice.qua+=quan;


        const products = JSON.parse(localStorage.getItem("iki-cart")); // get products from localstorge

        var index = _.findIndex(products, {id: item.id});   // Find product index in localstorge using _.findIndex

        products.splice(index, 1,item); // Replace product at index using native splice with new prop of same product

        const isFpound = _.find(item.product_prices,function(o) { return o.qua > 0; });

        if(!isFpound){

          for (let i = 0; i < products.length; i++) {
            if (products[i] === item) {
              products.splice(i, 1);
            }
          }

        }


        state.cartProducts = [];
        state.cartProducts = products

        //
        // commit('setPorducts', products);

    //    this.SET_CART_PRODUCTS(products);    // remove the caretProducts and set new new prop of same product

        localStorage.setItem("iki-cart", JSON.stringify(products)); // set new prop products to localstorge
        return products;
        },




        CALCULATE_TOTAL_PRICE:(state) => {
          state.orderMoney.sub_total=0;
          state.orderMoney.discount=0;
          state.cartProducts.forEach(product => {
            product.product_prices.forEach(price=>{
            state.orderMoney.sub_total += parseFloat(price.price*price.qua);
            });
          });
state.orderMoney.total=state.orderMoney.sub_total+state.orderMoney.shipping;
          },

        CALCULATE_TOTAL_AFTER_DICOUNT:(state,discount)=>{

          state.orderMoney.discount=discount;
          state.orderMoney.total=state.orderMoney.total-state.orderMoney.discount;


        },




        SET_ORDER_DETAILS:(state, details) => {
            state.orderDetails = details;
        },
        ADD_USER_ADDRESS: (state, address) => {
            state.useraddress = address;
            localStorage.setItem('address', JSON.stringify(address))
        },

        ADD_LOGGED_USER: (state, user) => {
            state.loggedUser = user;
            localStorage.setItem('_auth', encryptUser(user))
        },

        SET_CART_PRODUCTS: (state, products) => {
            state.cartProducts = [];
            state.cartProducts = products
        },
        SET_ORDER: (state)=>{

          // state.orderMoney.sub_total=state.orderMoney.sub_total;
          // state.orderMoney.total=state.orderMoney.total;
          // state.orderMoney.total=state.totalValue+state.deliveryCost;

          // state.orderMoney.shipping=state.deliveryCost;
          state.orderProducts=state.cartProducts;
          state.orderDetails.user_address_id=state.useraddress.id;
          state.orderDetails.delivery_ship_price=state.orderMoney.shipping;

          // console.log('orderMoney',state.orderMoney);
          // console.log('orderProducts',state.orderProducts);
          // console.log('orderDetails',state.orderDetails);
          // //
          // return;

          axios
            .post(`api/order`,{
              orderMoney:state.orderMoney,
              orderProducts:state.orderProducts,
              orderDetails:state.orderDetails

            })
            .then(response => {
              // this.showLoader = false;

              if(response.data.code==200){
              successToaster('order saved', "");
              restCart();
              console.log(response.data);
              }
              else if(response.data.code==201){
                successToaster('جارى التوجيه لدفع باى بال', "");
                location.replace(response.data.data);
                // window.open(response.data.data, '_blank');
              }
              else{errorToaster(response.data.message, "");}
            })
            .catch(error => {
              // this.showLoader = false;
              console.log(error);

              errorToaster('حدث خطأ ! تأكد من اتصالاك بالانترنت', "");

            });


        },

    },
    actions:{
      cal_total: ({commit}) =>{
        commit("CALCULATE_TOTAL_PRICE");
      },
      changeQuantity: ({commit},payLoad) =>{

        commit("CHANGE_QUANTITY",{item:payLoad.item,price:payLoad.price,quan:payLoad.quan});
        commit("CALCULATE_TOTAL_PRICE");
      },
    },

})
