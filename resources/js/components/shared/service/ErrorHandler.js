/* Handling Errors through Toastr */

export const successToaster = (title, desc) => {
  return Vue.toasted.success(title).goAway(2000);

}
export const errorToaster = (title, desc) => {

return Vue.toasted.error(title).goAway(2000);
}

export const warnToaster = (title, desc) => {
  return Vue.toasted.warning(title).goAway(2000);
}

export const infoToaster = (title, desc) => {
  return Vue.toasted.info(title).goAway(2000);
}
