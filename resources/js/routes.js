import Massenger from './components/chat/Massenger';
import Products from './views/Products.vue';
import About from './views/About.vue';
import login from './views/login.vue';
import Menue from './views/Menu.vue';
import ProductDetail from './components/products/ProductDetail.vue';
import CartProducts from './components/products/cart/CartProducts.vue';
import Home from './views/Home.vue';
import TrackOrder from './views/TrackOrder';
import MyOrders from './views/MyOrders';
import OrderDetails from './components/orders/OrderDetails';
import Addresses from './components/address/UserAdresses';
import Profile from './views/Profile';
import Checkout from './views/Checkout';
import OrderDone from './components/orders/OrderDone';
import Search from './views/Search';




import {
    isLoggedIn,isAddressSelected,cartProductsCount
} from "./components/shared/service/authService"

export const routes=[
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {path:'/massenger',component:Massenger},
    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component:About
    },
    {
        path: '/food_results',
        name: 'products',
        component: Products
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile
    },
    {
        path: '/products/:id',
        name: 'productDetails',
        component:ProductDetail
    },
    {
        path: '/track_order',
        name: 'TrackOrder',
        component:TrackOrder
    },
    {
        path: '/orders',
        name: 'MyOrders',
        component:MyOrders
    },
    {
        path: '/orderdone/:id/',
        name: 'OrderDone',
        component:OrderDone
    },

    {
        path: '/restaurants/',
        name: 'Menue',
        component:Menue
    },

    {
        path: '/search/',
        name: 'Search',
        component:Search,
        props:true
    },

    {
        path: '/order_details/:id/',
        name: 'OrderDetails',
        component:OrderDetails
    },

    {
        path: '/addresses',
        name: 'Addresses',
        component:Addresses,
        beforeEnter: (to, from, next) => {
            if (isLoggedIn())
                  next();
                  else{
                    next({
                        name: 'login',
                        query: {
                            from: to.name
                        }
                    })

                  }
              }

    },



    {
        path: '/cart',
        name: 'cart',
        component: CartProducts,
        beforeEnter: (to, from, next) => {
            if (isLoggedIn()) {
                if(isAddressSelected()==true){
                  next();

                }
                else{
                  next({
                      name: 'Addresses',
                      query: {
                          from: to.name
                      }
                  })

                }

            } else {
                next({
                    name: 'login',
                    query: {
                        from: to.name
                    }
                })
            }
        }
    },

    {
        path: '/checkout',
        name: 'checkout',
        component: Checkout,
        beforeEnter: (to, from, next) => {
            if (isLoggedIn()&&cartProductsCount()) {
                next();
            } else {
                next({
                    name: 'login',
                    query: {
                        from: to.name
                    }
                })
            }
        }
    },




    {
        path: '/login',
        name: 'login',
        component: login,
        beforeEnter: (to, from, next) => {
            if (isLoggedIn()) {
              next({
                  name: 'home',
                  query: {
                      from: to.name
                  }
              })
            } else {
              next();
            }
        }


    }

];
