
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('./assets/js/jquery.min.js');

    // require('bootstrap');
    require('./assets/js/bootstrap.min.js');

} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}





/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';
// //
window.Pusher = require('pusher-js');
window.Pusher.logToConsole = true;
// //
// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     //wsPort: 6001,
//     encrypted: true
// });


// import './assets/js/jquery.min.js';
// import './assets/js/bootstrap.min.js';

// require('./assets/js/jquery.min.js');
// require('./assets/js/tether.min.js');
// require('./assets/js/bootstrap-slider.min.js');
// require('./assets/js/jquery.isotope.min.js');
// require('./assets/js/headroom.js');
// require('./assets/js/foodpicky.js');
//
//
// require('./assets2/js/jquery-migrate-3.0.1.min.js');
// require('./assets2/js/popper.min.js');
// require('./assets2/js/jquery.easing.1.3.js');
// require('./assets2/js/jquery.waypoints.min.js');
// require('./assets2/js/jquery.stellar.min.js');
// require('./assets2/js/owl.carousel.min.js');
// require('./assets2/js/jquery.magnific-popup.min.js');
// require('./assets2/js/aos.js');
// require('./assets2/js/jquery.animateNumber.min.js');
// require('./assets2/js/jquery.timepicker.min.js');
// require('./assets2/js/scrollax.min.js');
//





 // import './assets/js/jquery.min.js';
// import './assets/js/tether.min.js';
// import './assets/js/bootstrap.min.js';
// import './assets/js/bootstrap-slider.min.js';
// import './assets/js/jquery.isotope.min.js';
// import './assets/js/headroom.js';
// import './assets/js/foodpicky.js';
