
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue';
import store from './store'
import {routes} from "./routes";
import Toasted from "vue-toasted";

// import 'bootstrap'
var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);

import './assets/css/bootstrap.min.css';
import './assets/css/custom_bootstrap.css';
// import 'bootstrap/dist/css/bootstrap.css';

import './assets/css/font-awesome.min.css';
import './assets/css/animate.css';
import './assets/css/animsition.min.css';
import './assets/css/style.css';



// import './assets2/css/open-iconic-bootstrap.min.css';
// import './assets2/css/animate.css';
// // import './assets2/css/owl.carousel.min.css';
// import './assets2/css/owl.theme.default.min.css';
// import './assets2/css/magnific-popup.css';
//
// import './assets2/css/aos.css';
// import './assets2/css/ionicons.min.css';
// // import './assets2/css/bootstrap-datepicker.css';
// import './assets2/css/jquery.timepicker.css';
// import './assets2/css/flaticon.css';
// import './assets2/css/icomoon.css';
// import './assets2/css/style.css';


//



Vue.use(Toasted,{
    iconPack : 'fontawesome'});

    Vue.toasted.register('my_app_error', 'Oops.. Something Went Wrong..', {
        type : 'error',
        icon : 'error_outline'
    })

// import Apps from "./App.vue";
// Vue.component('Headers', Apps);

import Header from './components/shared/commen/Header';
Vue.component('Headers',Header);


import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueChatScroll from 'vue-chat-scroll';
Vue.use(VueChatScroll);

import Vuex from 'vuex'
Vue.use(Vuex);
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import { faUser, faBuilding } from '@fortawesome/free-solid-svg-icons';
import { faUserCircle, faCheckCircle } from '@fortawesome/free-regular-svg-icons';

library.add(
    faUser,
    faBuilding,
    faUserCircle,
    faCheckCircle,
);

Vue.component('font-awesome-icon', FontAwesomeIcon); // registered globally

import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
Vue.component('icon', Icon);


const router=new VueRouter({routes});


require('./bootstrap');





window.Vue = require('vue');

// Vue.config.productionTip  =  false
//
// import {
//     decryptUser
// } from './components/shared/service/authService'
//
// const  accessToken  =  this.decryptUser();
//
//
// if (accessToken) {
//     Vue.prototype.$http.defaults.headers.common['Authorization'] =  accessToken.message.token
// }
//
// console.log('aaaaa',accessToken);

// Vue.component('order-progress', require('./components/orders/OrderStatus.vue'));


import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBfs0ijH-s84YElMdmuuYr084Td4q3vE_c',
  },
});
import {isLoggedIn,  getLoggedInUser } from "./components/shared/service/authService";

import  Axios  from  'axios';

const app = new Vue({
    el: '#app',
    data:{
      headers:''
    },
    created (){
      const loggedUser = getLoggedInUser();
      if(loggedUser){this.headers=loggedUser.token;}
      console.log(loggedUser);
      Axios.defaults.headers = {
             'Content-Type': 'application/json',
             Authorization: "Bearer "+this.headers
         }

    },


// created (){
//
//   this.pusher = new Pusher('1a041caa97b6db9892dd', {
//     cluster: 'us2',
//     encrypted: true
//   })
//   this.pusherChannel = this.pusher.subscribe('tracker');
//
//   this.pusherChannel.bind('App\\Events\\OrderStatusChange', function(data) {
//       console.log('message received');
//       console.log(data);
//   }.bind(this));
//
// },


  //   created (){
  //     Echo.channel('tracker')
  //     .listen('App\\Events\\OrderStatusChange', (e) => {
  //       console.log('omgggg realtime bro')
  //     });
  // },
    router,
    store,
    mode:'history'
});
