<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- <title>{{ config('app.name', 'Online Order') }}</title> -->
    <title>Online Ordering</title>

    <!-- Scripts -->

    <!-- Fonts -->

    {{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}


    {{--<link rel="stylesheet" href="{{'assets/css/bootstrap.min.css'}}">--}}
{{--<link rel="stylesheet" href="{{'assets/css/font-awesome.min.css'}}">--}}
{{--<link rel="stylesheet" href="{{'assets/css/animate.css'}}">--}}
{{--<link rel="stylesheet" href="{{'assets/css/animsition.min.css'}}">--}}
    {{--<link rel="stylesheet" href="{{'assets/css/style.css'}}">--}}
</head>
<body>
    <div id="app">
      <Headers></Headers>

            @yield('content')

    </div>

    {{--<script type="text/javascript" src="{{'assets/js/jquery.min.js'}}"></script>--}}
    {{--<script type="text/javascript" src="{{'assets/js/tether.min.js'}}"></script>--}}
    {{--<script type="text/javascript" src="{{'assets/js/bootstrap.min.js'}}"></script>--}}
    {{--<script type="text/javascript" src="{{'assets/js/bootstrap-slider.min.js'}}"></script>--}}
    {{--<script type="text/javascript" src="{{'assets/js/jquery.isotope.min.js'}}"></script>--}}
    {{--<script type="text/javascript" src="{{'assets/js/jquery.headroom.js'}}"></script>--}}
    {{--<script type="text/javascript" src="{{'assets/js/jquery.foodpicky.min.js'}}"></script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfs0ijH-s84YElMdmuuYr084Td4q3vE_c"></script>

    <script type="text/javascript" src="{{ asset('js/app.js') }}" defer></script>

</body>
</html>
