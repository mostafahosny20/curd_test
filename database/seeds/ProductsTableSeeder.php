<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $products = [
        [
          'cat_id'=>1,
            'name_ar' => "MEN'S BETTER THAN NAKED & JACKET",
            'name_en' => "MEN'S BETTER THAN NAKED & JACKET",
            'desc_ar' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'desc_en' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'image' => 'http://images.thenorthface.com/is/image/TheNorthFace/236x204_CLR/mens-better-than-naked-jacket-AVMH_LC9_hero.png',
            'created_at' => new DateTime,
            'updated_at' => null,
        ],
        [
          'cat_id'=>1,
            'name_ar' => "MEN'S BETTER THAN NAKED & JACKET",
            'name_en' => "MEN'S BETTER THAN NAKED & JACKET",
            'desc_ar' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'desc_en' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'image' => 'http://images.thenorthface.com/is/image/TheNorthFace/236x204_CLR/mens-better-than-naked-jacket-AVMH_LC9_hero.png',
            'created_at' => new DateTime,
            'updated_at' => null,
        ],
        [
          'cat_id'=>1,
            'name_ar' => "MEN'S BETTER THAN NAKED & JACKET",
            'name_en' => "MEN'S BETTER THAN NAKED & JACKET",
            'desc_ar' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'desc_en' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'image' => 'http://images.thenorthface.com/is/image/TheNorthFace/236x204_CLR/mens-better-than-naked-jacket-AVMH_LC9_hero.png',
            'created_at' => new DateTime,
            'updated_at' => null,
        ],
        [
          'cat_id'=>1,
            'name_ar' => "MEN'S BETTER THAN NAKED & JACKET",
            'name_en' => "MEN'S BETTER THAN NAKED & JACKET",
            'desc_ar' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'desc_en' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'image' => 'http://images.thenorthface.com/is/image/TheNorthFace/236x204_CLR/mens-better-than-naked-jacket-AVMH_LC9_hero.png',
            'created_at' => new DateTime,
            'updated_at' => null,
        ],
        [
          'cat_id'=>1,
            'name_ar' => "MEN'S BETTER THAN NAKED & JACKET",
            'name_en' => "MEN'S BETTER THAN NAKED & JACKET",
            'desc_ar' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'desc_en' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua consequat.',

            'image' => 'http://images.thenorthface.com/is/image/TheNorthFace/236x204_CLR/mens-better-than-naked-jacket-AVMH_LC9_hero.png',
            'created_at' => new DateTime,
            'updated_at' => null,
        ],


];

DB::table('products')->insert($products);


        //
    }
}
